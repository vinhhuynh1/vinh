const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  content: [
    './src/components/**/*.{ts,tsx,js,jsx}',
    './src/pages/**/*.{ts,tsx,js,jsx}',
  ],
  theme: {
    screens: {
      xs: '428px',
      ...defaultTheme.screens,
    },

    container: {
      center: true,

      padding: {
        DEFAULT: '1rem',
        lg: '4.5rem',
        '2xl': '6rem',
      },
    },

    extend: {
      screens: {
        '2xl': '1440px',
      },

      colors: {
        primary: '#1DE9B6',
        secondary: '#2E4FFF',
        tertiary: '#BF426F',
        white: '#FFFFFF',
        gray: '#dedede',
      },
      fontFamily: {
        airStrike: ['Airstrike', ...defaultTheme.fontFamily.sans],
        airStrikeHalf: ['Airstrike-Half', ...defaultTheme.fontFamily.sans],
        TTNormsPro: ['TT Norms Pro', ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        'header-1': ['3.75rem', '6rem'],
        'header-2': ['3.125rem', '4.6875rem'],
        'header-3': ['2.25rem', '3.375rem'],
        'header-4': ['1.5rem', '3.75rem'],
        'header-5': ['1.25rem', '1.875rem'],

        'body-1': ['1.75rem', '2.9375rem'],
        'body-2': ['1.5rem', '2.5rem'],
        'body-3': ['1.25rem', '2.125rem'],
        'body-4': ['1.0625rem', '1.8125rem'],
        'body-5': ['0.9375rem', '1.5625rem'],
        'body-6': ['0.8125rem', '1.375rem'],
      },
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--gradient-color-stops))',
      },
    },
  },
  plugins: [
    require('@tailwindcss/line-clamp'),
    require('@tailwindcss/aspect-ratio'),
  ],
}
