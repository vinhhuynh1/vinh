import { ReactNode } from 'react'

export type Statistic = {
  growth: string
  count: string
  unit: string
  icon: ReactNode
}

export type Team = {
  name: string
  position: string
  image: string
  twitter?: string
  discord?: string
  telegram?: string
}

export interface MapItem {
  title: string
  desc: string[]
}
