import classNames from 'classnames'
import React from 'react'

interface DotsProps {
  random?: boolean
  className?: string
}

const Dots: React.FC<DotsProps> = ({ random = false, className }) => {
  return random ? (
    <div
      className={classNames(
        className,
        'inline-flex h-3 w-[110px] flex-nowrap items-center justify-between'
      )}
    >
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.8s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.6s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.4s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.2s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite] bg-[#00E4FF]"></div>
    </div>
  ) : (
    <div
      className={classNames(
        className,
        'inline-flex h-3 w-[110px] flex-nowrap items-center justify-between'
      )}
    >
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.2s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.8s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.6s] bg-[#00E4FF]"></div>
      <div className="h-3 w-3 animate-[fade_1.2s_ease-in-out_infinite_-0.4s] bg-[#00E4FF]"></div>
    </div>
  )
}

export default Dots
