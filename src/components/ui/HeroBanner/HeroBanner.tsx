import AppleIcon from '@/components/icons/AppleIcon'
import CHPlayIcon from '@/components/icons/CHPlayIcon'
import React from 'react'
import { Button } from '../Button'
import Image from '../Image'
import Section from '../Section'
import HeroBannerModol from './HeroBannerModol'
import { Chip } from '@material-tailwind/react'

// interface HeroBannerProps {}
interface HeroBannerProps {
  id: string
  img: string
  Card: string
  Title: string
  Date: string
  imageAvata: string
  nameAvata: string
  ImpormationAvata: string
  money: string
  money1: string
}
const backerList = [
  {
    id: 1,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 2,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 3,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 4,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 5,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 6,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 7,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 8,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 9,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 10,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 11,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
  {
    id: 12,
    img: '/litepaper/icons/anh1.svg',
    Card: '',
    Title: 'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps',
    Date: ' Tue, 30 Aug 2022 | 10:00 - 12:00 WIB',
    imageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Dias Wisnu',
    ImpormationAvata: 'Instructor',
    money: 'Rp 50.000.000',
    money1: 'Rp 50.000.000',
  },
]
console.log(backerList, 'backerList')
const HeroBanner: React.FC<HeroBannerProps> = () => {
  return (
    <Section >
      <div className="flex flex-wrap justify-around  ">
        {backerList.map((mode) => {
          return (
            <HeroBannerModol
              key={mode.Title}
              Title={mode.Title}
              img={mode.img}
              Date={mode.Date}
              imageAvata={mode.imageAvata}      
              nameAvata={mode.nameAvata}
              ImpormationAvata={mode.ImpormationAvata}
              money={mode.money}
              money1={mode.money1}
            />
          )
        })}
      </div>
    </Section>
  )
}

export default HeroBanner
