import Image from '@/components/ui/Image'
import classNames from 'classnames'
import React from 'react'
import { Chip } from '@material-tailwind/react'
import styles from './RunningMode.module.css'

interface HeroBannerModol {
  id: string
  img: string
  Card: string
  Title: string
  Date: string
  imageAvata: string
  nameAvata: string
  ImpormationAvata: string
  money: string
  money1: string
}

const HeroBannerModol: React.FC<HeroBannerModol> = ({
  id,
  img,
  Card,
  Title,
  Date,
  imageAvata,
  nameAvata,
  ImpormationAvata,
  money,
  money1,
}) => {
  return (
    <div className="border-gray-200 dark:bg-gray-800 dark:border-gray-700 max-w-sm rounded-lg border bg-white shadow-md mb-2.5">
      <div className="relative lg:h-[200px] lg:w-[auto] 2xl:h-[220px] 2xl:w-[auto]">
        <Image src={img} layout="fill" />
      </div>
      <div className="bg-white px-5">
        <div className="flex w-96 flex-wrap justify-center">
          <div className="chip chip-blue mr-2 w-20 rounded-xl bg-red-500 text-center">
            Webinar
          </div>
          <div className="chip chip-green mr-2 w-20 rounded-xl bg-sky-900 text-center">
            Certified
          </div>
          <div className="chip chip-red mr-2 w-20 rounded-xl bg-amber-600 text-center">
            1200 Point
          </div>
          <div className="chip chip-orange mr-2 w-20 rounded-xl bg-green-500 text-center">
            5 more
          </div>
          {/* <div className="chip chip-purple bg-black rounded-xl w-20 text-center">chip purple</div> */}
        </div>
        <a href="#">
          <div className="mb-2 text-xl font-black text-black">{Title}</div>
        </a>
        <div className="text-gray-700 mb-3 font-normal text-black">{Date}</div>
      </div>

      <div className="flex ml-5">
        <img className="" src={imageAvata} />
        <div className="">
          <div className="mb-2 text-xl font-black text-black">{nameAvata}</div>
          <div className="mb-2 text-xl font-black text-black">
            {ImpormationAvata}
          </div>
        </div>
      </div>

      <div className="flex justify-between ml-5">
        <div className="mb-2 text-xl font-black text-black">{money}</div>
        <div className="mb-2 text-xl font-black text-black line-through ">{money1}</div>
      </div>
    </div>
  )
}

export default HeroBannerModol
