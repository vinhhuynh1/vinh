import AppleIcon from '@/components/icons/AppleIcon'
import CHPlayIcon from '@/components/icons/CHPlayIcon'
import React from 'react'
import { Button } from '../Button'
import Image from '../Image'
import Section from '../Section'
import HeroBannerModol from './HeroBannerModol'
import { Chip } from '@material-tailwind/react'
import classNames from 'classnames'
import { color } from 'echarts'

interface HeroBannerProps {
  text: string
  color: "warning" | "info" | "error"
}


const HeroBanner = (props: HeroBannerProps) => {
  return (
            <div className={classNames("chip chip-blue mr-2 w-20 rounded-xl bg-red-500 text-center", {
                "bg-red-400": props.color === "warning",
                "bg-blue-90": props.color === "info",
                "bg-yellow-500": props.color === "error"
                
            })}>
              {props.text}
            </div>
  )
}

export default HeroBanner
