import React from 'react'
import Image from '../Image'
import Section from '../Section'

interface TopBannerProps {
  title: string
  highlight: string
  description: string
}

const TopBanner: React.FC<TopBannerProps> = ({
  title,
  highlight,
  description,
}) => {
  return (
    <Section className="mt-6">
      <div className="flex flex-row items-center">
        <div className="lg:w-3/5">
          <h1 className="lg:text-[40px] lg:leading-[60px] 2xl:text-[56px] 2xl:leading-[90px]">
            {title}{' '}
            <span
              className="pr-2 text-transparent text-gradient bg-clip-text font-airStrike"
              dangerouslySetInnerHTML={{ __html: highlight }}
            ></span>
          </h1>
          <p
            dangerouslySetInnerHTML={{ __html: description }}
            className="italic lg:text-xs lg:leading-5 2xl:text-base"
          ></p>
        </div>
        <div className="relative flex justify-end lg:w-2/5">
          <div className="relative lg:h-[330px] lg:w-[359px] 2xl:h-[445px] 2xl:w-[483px]">
            <Image
              src="/page-banner-model.png"
              alt="banner image"
              layout="fill"
              unoptimized={true}
            />
            <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[120%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
          </div>
        </div>
      </div>
    </Section>
  )
}

export default React.memo(TopBanner)
