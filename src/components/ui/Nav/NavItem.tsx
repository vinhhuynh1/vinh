import Link, { LinkProps } from 'next/link'
import { useRouter } from 'next/router'
import React, { useMemo } from 'react'

interface NavItemProps extends LinkProps {
  children(props: { isActive: boolean }): React.ReactNode
  className?: string
  onClick?: () => void
}

const NavItem: React.FC<NavItemProps> = ({ onClick, ...props }) => {
  const router = useRouter()
  const isActive = useMemo(
    () => router.route === props.href,
    [router.route, props.href]
  )

  return (
    <Link {...props}>
      <a className={props.className} onClick={onClick}>
        {props.children({ isActive })}
      </a>
    </Link>
  )
}

export default NavItem
