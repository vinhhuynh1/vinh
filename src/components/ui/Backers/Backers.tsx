import { useWindowDimensions } from '@/hooks'
import {
  Babylons,
  BinanceNFT,
  BLabs,
  CertiK,
  Coin68,
  Defily,
  GameFi,
  KrystalGO,
  Liquidifty,
  NFTb,
  RedKite,
  Refinable,
} from 'public/backers'
import React from 'react'
import { Else, If, Then } from 'react-if'
import Image from '../Image'
import Section from '../Section'
import SectionHeading from '../SectionHeading'

interface BackersProps {}

const backerList = [
  {
    name: 'RedKite',
    logo: RedKite,
  },
  {
    name: 'CertiK',
    logo: CertiK,
  },
  {
    name: 'GameFi',
    logo: GameFi,
  },
  {
    name: 'Refinable',
    logo: Refinable,
  },
  {
    name: 'Liquidifty',
    logo: Liquidifty,
  },
  {
    name: 'Babylons',
    logo: Babylons,
  },
  {
    name: 'Babylons-2',
    logo: Babylons,
  },
  {
    name: 'NFTb',
    logo: NFTb,
  },
  {
    name: 'KrystalGO',
    logo: KrystalGO,
  },
  {
    name: 'Coin68',
    logo: Coin68,
  },
  {
    name: 'BinanceNFT',
    logo: BinanceNFT,
  },
  {
    name: 'BLabs',
    logo: BLabs,
  },
  {
    name: 'Defily',
    logo: Defily,
  },
]

const Backers: React.FC<BackersProps> = () => {
  const { width } = useWindowDimensions()

  return (
    <Section hasPadding className="mt-[72px] xs:mb-16 lg:mb-20 2xl:mb-[130px]">
      <SectionHeading
        title="our backers"
        desc="We believe in constant innovation and place great importance in forming and maintaining strong partnerships. 
        Contact to bringing new possibilities with us."
      />
      <div className="lg:mt-6  2xl:mt-[90px] ">
        <If condition={width >= 1024}>
          <Then>
            <div className="mx-auto flex items-center justify-around lg:max-w-[622px] 2xl:max-w-[1065px]">
              {backerList.slice(0, 6).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
            <div className="flex items-center justify-around lg:mt-6 2xl:mt-10">
              {backerList.slice(6).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
          </Then>
          <Else>
            <div className="flex items-center justify-around mx-auto">
              {backerList.slice(0, 2).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
            <div className="flex items-center justify-around">
              {backerList.slice(2, 5).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
            <div className="flex items-center justify-around">
              {backerList.slice(5, 9).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
            <div className="flex items-center justify-around">
              {backerList.slice(9, 13).map((backer) => (
                <div key={backer.name}>
                  <Image src={backer.logo} alt={backer.name} />
                </div>
              ))}
            </div>
          </Else>
        </If>
      </div>
    </Section>
  )
}

export default Backers
