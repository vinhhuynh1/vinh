import classNames from 'classnames'
import React from 'react'

export interface SectionProps {
  className?: string
  children?: React.ReactNode
  isLoading?: boolean
  hasPadding?: boolean
}

const Section = React.forwardRef<HTMLDivElement, SectionProps>(
  ({ children, className, hasPadding = true }, ref) => {
    return (
      <div
        ref={ref}
        className={classNames(hasPadding && 'container', className)}
      >
        {children}
      </div>
    )
  }
)
Section.displayName = 'Section'

export default React.memo(Section)
