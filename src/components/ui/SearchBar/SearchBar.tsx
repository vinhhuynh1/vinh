import { SearchIcon } from '@/components/icons'
import { useDebounce } from '@/hooks'
import classNames from 'classnames'
import { useRouter } from 'next/router'
import React from 'react'

interface SearchBarProps {
  className?: string
}

const SearchBar: React.FC<SearchBarProps> = ({ className }) => {
  const [searchValue, setSearchValue] = React.useState<string>('')
  const [searchResult, setSearchResult] = React.useState<string[]>([])
  const debouncedValue = useDebounce(searchValue, 500)
  const router = useRouter()

  React.useEffect(() => {
    if (!debouncedValue.trim()) {
      setSearchResult([])
      return
    }
    console.log(debouncedValue)
    const fetchApi = async () => {}
    fetchApi()
  }, [debouncedValue])

  const handleChange = (e: React.KeyboardEvent<HTMLInputElement>) => {
    e.preventDefault()

    const q = e.currentTarget.value

    if (!q.startsWith(' ')) {
      setSearchValue(q)
    }

    router.push(
      {
        pathname: '/whitepaper',
        query: q ? { q } : {},
      },
      undefined,
      { shallow: true }
    )
  }

  return (
    <div className={classNames('', className)}>
      <div className="relative py-3 border-b border-white">
        <SearchIcon className="absolute -translate-y-1/2 top-1/2" />
        <input
          type="text"
          placeholder="Search"
          className="min-w-[203px] border-none bg-transparent pl-9 placeholder-white outline-none"
          onChange={handleChange}
        />
      </div>
    </div>
  )
}

export default React.memo(SearchBar)
