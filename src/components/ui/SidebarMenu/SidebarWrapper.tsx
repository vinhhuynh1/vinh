import classNames from 'classnames'
import React from 'react'

interface SidebarWrapperProps {
  className?: string
  children: React.ReactNode
}

const SidebarWrapper: React.FC<SidebarWrapperProps> = ({
  className,
  children,
}) => {
  return <div className={classNames(className)}>{children}</div>
}

export default SidebarWrapper
