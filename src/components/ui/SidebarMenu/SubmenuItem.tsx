import classNames from 'classnames'
import Link from 'next/link'
import React from 'react'
import styles from './Submenu.module.css'

export interface SubmenuItemProps {
  title: string
  href?: string
}

const SubmenuItem: React.FC<SubmenuItemProps> = ({ title, href }) => {
  return (
    <li
      className={classNames(
        styles.container,
        'relative pt-[10px] pb-[10px] pl-[36px] pr-[22px] after:absolute after:top-1/2 after:left-2 after:h-2 after:w-2 after:-translate-y-1/2 after:rounded-full'
      )}
    >
      <Link href={href}>
        <a className={classNames('relative block')}>
          <span className="text-sm italic font-TTNormsPro">{title}</span>
        </a>
      </Link>
    </li>
  )
}

export default SubmenuItem
