import classNames from 'classnames'
import React from 'react'

interface MenuListProps {
  className?: string
  children: React.ReactNode
}

const MenuList: React.FC<MenuListProps> = ({ className, children }) => {
  return <ul className={classNames('', className)}>{children}</ul>
}

export default MenuList
