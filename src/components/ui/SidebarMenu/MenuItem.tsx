import { ChevronDownIcon } from '@/components/icons'
import classNames from 'classnames'
import Link from 'next/link'
import React from 'react'
import MenuList from './MenuList'
import SubmenuItem, { SubmenuItemProps } from './SubmenuItem'

interface MenuItemProps {
  item: {
    title: string
    href?: string
    children?: SubmenuItemProps[]
  }
  className?: string
}

const MenuItem: React.FC<MenuItemProps> = ({ item, className }) => {
  const [isOpen, setIsOpen] = React.useState(false)

  return (
    <li className={classNames('', className)}>
      {item.children ? (
        <div className="cursor-pointer" onClick={() => setIsOpen(!isOpen)}>
          <div className="flex items-center justify-between">
            <span className="italic font-TTNormsPro text-body-4">
              {item.title}
            </span>
            <ChevronDownIcon />
          </div>
          {isOpen && (
            <MenuList className="mt-4 space-y-5 ">
              {item.children.map((child) => (
                <SubmenuItem
                  href={child.href}
                  title={child.title}
                  key={child.title}
                />
              ))}
            </MenuList>
          )}
        </div>
      ) : (
        <Link href="#!">
          <a className="block italic font-TTNormsPro text-body-4">
            {item.title}
          </a>
        </Link>
      )}
    </li>
  )
}

export default MenuItem
