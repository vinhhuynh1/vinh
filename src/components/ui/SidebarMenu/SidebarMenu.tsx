import React from 'react'
import MenuItem from './MenuItem'
import MenuList from './MenuList'
import SidebarWrapper from './SidebarWrapper'

interface SidebarMenuProps {}

const data = [
  {
    title: 'Overview',
    href: '#!',
  },
  {
    title: 'Getting Started',
    href: '#!',
    children: [
      {
        title: 'Introduction',
        href: '#!',
      },
    ],
  },
  {
    title: 'Game-Fi Systems',
    children: [
      {
        title: 'Earning Cap/Mechanics',
        href: '#!',
      },
      {
        title: 'Energy System',
        href: '#!',
      },
      {
        title: 'Quest System',
        href: '#!',
      },
      {
        title: '1111111Achievement System',
        href: '#!',
      },
      {
        title: 'Anti-Cheating System',
        href: '#!',
      },
      {
        title: 'Tax and Fee System',
        href: '#!',
      },
    ],
  },
  {
    title: 'Other systems',
    href: '#!',
  },
  {
    title: 'Marketplace',
    href: '#!',
  },
]

const SidebarMenu: React.FC<SidebarMenuProps> = () => {
  return (
    <>
      <SidebarWrapper className="mt-5">
        <MenuList className="space-y-5">
          {data.map((item) => (
            <MenuItem item={item} key={item.title} />
          ))}
        </MenuList>
      </SidebarWrapper>
    </>
  )
}

export default SidebarMenu
