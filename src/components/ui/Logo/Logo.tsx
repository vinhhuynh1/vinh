import classNames from 'classnames'
import React from 'react'
import Image from '../Image'

const Logo: React.FC<React.HtmlHTMLAttributes<HTMLDivElement>> = ({
  className,
  ...props
}) => {
  return (
    <div
      className={classNames(
        'flex items-center xs:space-x-[10px] 2xl:space-x-4',
        className
      )}
      {...props}
    >
      <div className="relative xs:h-[38px] xs:w-[58px] lg:h-[41px] lg:w-[68px] 2xl:h-[57px] 2xl:w-[86px]">
        <Image src="/logo.png" layout="fill" objectFit="contain" alt="logo" />
      </div>
      <div className="">
        <div className="text-gradient bg-clip-text pr-4 font-airStrikeHalf text-transparent xs:text-[30px] xs:leading-[25px] lg:text-[32px] lg:leading-[26px] 2xl:text-[44px] 2xl:leading-[37px]">
          P-GO
        </div>
        <div
          className="font-TTNormsPro font-bold italic text-[#CCFDF7] xs:text-[6px] xs:leading-[9px] lg:text-[7px] 2xl:text-[9px]
2xl:leading-[14px]"
        >
          Move, connect and earn
        </div>
      </div>
    </div>
  )
}

export default React.memo(Logo)
