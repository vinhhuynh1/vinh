import React from 'react'
import {
  Swiper as ReactSwiper,
  SwiperSlide as ReactSwiperSlide,
} from 'swiper/react'
import SwiperCore, { Pagination, Navigation, Controller } from 'swiper'

import 'swiper/css'
// import 'swiper/css/navigation'
import 'swiper/css/pagination'

import classNames from 'classnames'
import { SwiperNext, SwiperPrev } from '@/components/icons'
import Image from '../Image'

export interface SwiperProps extends React.ComponentProps<typeof ReactSwiper> {
  hideNavigation?: boolean
  hidePagination?: boolean
  isOverflowHidden?: boolean
}

// SwiperCore.use([Navigation, Pagination, Controller])

const Swiper: React.FC<SwiperProps> = ({
  children,
  hideNavigation,
  hidePagination,
  isOverflowHidden = false,
  className,
  ...props
}) => {
  const prevButtonRef = React.useRef<HTMLButtonElement>(null)
  const nextButtonRef = React.useRef<HTMLButtonElement>(null)

  return (
    <ReactSwiper
      className={classNames(
        isOverflowHidden ? '!overflow-hidden' : '!overflow-visible',
        className
      )}
      modules={[Navigation, Pagination, Controller]}
      pagination={!hidePagination && true}
      navigation={{
        prevEl: prevButtonRef.current ? prevButtonRef.current : undefined,
        nextEl: nextButtonRef.current ? nextButtonRef.current : undefined,
      }}
      onBeforeInit={(swiper) => {
        // @ts-ignore
        // eslint-disable-next-line no-param-reassign
        swiper.params.navigation.prevEl = prevButtonRef.current
        // @ts-ignore
        // eslint-disable-next-line no-param-reassign
        swiper.params.navigation.nextEl = nextButtonRef.current
        swiper.navigation.update()
      }}
      {...props}
    >
      {children}
      {!hideNavigation && (
        <>
          <button
            className="swiper-button-prev absolute top-1/2 left-0 z-10 -translate-y-1/2 cursor-pointer lg:h-10 lg:w-10 2xl:h-[60px] 2xl:w-[60px]"
            ref={prevButtonRef}
          >
            <SwiperPrev />
          </button>
          <button
            className="swiper-button-next absolute top-1/2 right-0 z-10 -translate-y-1/2 cursor-pointer lg:h-10 lg:w-10 2xl:h-[60px] 2xl:w-[60px]"
            ref={nextButtonRef}
          >
            <SwiperNext />
          </button>
        </>
      )}
    </ReactSwiper>
  )
}

export const SwiperSlide = ReactSwiperSlide

export default Swiper
