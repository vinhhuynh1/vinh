import React from 'react'
import * as echarts from 'echarts'
import type { EChartsOption, ECharts, SetOptionOpts } from 'echarts'

interface EChartProps {
  option: EChartsOption
}

const EChart: React.FC<EChartProps> = ({ option }) => {
  const chartRef = React.useRef<HTMLDivElement>(null)
  console.log('chartRef', chartRef)

  React.useEffect(() => {
    // Initialize chart
    let chart: ECharts | undefined
    if (chartRef.current !== null) {
      console.log(chartRef.current)
      chart = echarts.init(chartRef.current)
    }

    // Add chart resize listener
    // ResizeObserver is leading to a bit janky UX
    function resizeChart() {
      chart?.resize()
      console.log('run')
    }
    window.addEventListener('resize', resizeChart)

    // Return cleanup function
    return () => {
      chart?.dispose()
      window.removeEventListener('resize', resizeChart)
    }
  }, [])

  React.useEffect(() => {
    // Update chart
    if (chartRef.current !== null) {
      const chart = echarts.getInstanceByDom(chartRef.current)
      chart.setOption(option)
    }
  }, [option])

  return <div ref={chartRef} className="w-full h-full"></div>
}

export default EChart
