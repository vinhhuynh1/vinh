import * as React from 'react'
import ReactSelect, { SingleValue, StylesConfig, Props } from 'react-select'

export interface OptionsProps {
  readonly value: string
  readonly label: React.ComponentType<{ className: string }> | any
}

interface SelectOptionProps extends Props {
  options: OptionsProps[]
  customStyles?: StylesConfig<OptionsProps, false>
  onChange: (value: SingleValue<OptionsProps>) => void
  id?: string
  instanceId: string
}

const SelectOption: React.FC<SelectOptionProps> = (props) => {
  const { options, customStyles, onChange, ...rest } = props

  return (
    <ReactSelect
      options={options}
      styles={customStyles}
      onChange={onChange}
      {...rest}
    />
  )
}

export default React.memo(SelectOption)
