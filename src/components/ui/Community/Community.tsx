import { FacebookIcon, TelegramIcon, TwitterIcon } from '@/components/icons'
import React from 'react'
import { Button } from '../Button'
import Section from '../Section'
import SectionHeading from '../SectionHeading'

interface CommunityProps {}

const Community: React.FC<CommunityProps> = () => {
  return (
    <Section
      className="bg-[rgba(9,2,56,0.5)] xs:pt-5 xs:pb-11 lg:pt-6 lg:pb-[48px] 2xl:pt-10 2xl:pb-16"
      hasPadding={false}
    >
      <SectionHeading
        title="community"
        desc="If you are a fitness enthusiast looking to boost your income, join today to connect with P-Go community!"
      />
      <div className="flex items-center justify-center mt-8 xs:flex-col xs:space-y-8 lg:flex-row lg:space-y-0 lg:space-x-12">
        <Button
          className="flex min-w-[150px] items-center justify-center text-sm leading-[28px]"
          iconClassName="mr-2 w-5 h-5"
          primary
          LeftIcon={FacebookIcon}
        >
          Facebook
        </Button>
        <Button
          className="flex min-w-[150px] items-center justify-center text-sm leading-[28px]"
          iconClassName="mr-2 w-5 h-5"
          primary
          LeftIcon={TwitterIcon}
        >
          Twitter
        </Button>
        <Button
          className="flex min-w-[150px] items-center justify-center text-sm leading-[28px]"
          iconClassName="mr-2 w-5 h-5"
          primary
          LeftIcon={TelegramIcon}
        >
          Telegram
        </Button>
      </div>
    </Section>
  )
}

export default React.memo(Community)
