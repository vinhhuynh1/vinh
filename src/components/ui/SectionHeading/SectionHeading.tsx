import React from 'react'

interface SectionHeadingProps {
  title: string
  desc?: string
}

const SectionHeading: React.FC<SectionHeadingProps> = ({ title, desc }) => {
  return (
    <div className="">
      <div className="flex items-center">
        <div className="h-[3px] flex-1 bg-gradient-to-l from-[#4FAEC5] via-[#2D65B8E0] to-[#18275E40]" />
        <h2 className="text-gradient relative bg-clip-text px-5 text-transparent xs:text-[32px] xs:leading-[56px] lg:text-[40px] lg:leading-[56px] 2xl:text-header-1">
          {title}
        </h2>
        <div className="h-[3px] flex-1 bg-gradient-to-r from-[#4FAEC5] via-[#2D65B8E0] to-[#18275E40]" />
      </div>
      {desc && (
        <p className="mx-auto text-center font-normal italic text-gray xs:text-xs xs:leading-5 lg:mt-1 lg:max-w-[512px] 2xl:mt-4 2xl:max-w-[640px] 2xl:text-base 2xl:leading-[26px]">
          {desc}
        </p>
      )}
    </div>
  )
}

export default SectionHeading
