import classNames from 'classnames'
import React from 'react'
import BaseButton, { BaseButtonProps } from '@/components/ui/Button/BaseButton'

import styles from './Button.module.css'

interface ButtonProps extends BaseButtonProps {
  secondary?: boolean
}

const Button: React.FC<ButtonProps> = ({
  className,
  children,
  secondary,
  ...props
}) => {
  return (
    <BaseButton
      type="button"
      className={classNames(
        'before:btn-shape relative z-10 py-3 font-TTNormsPro before:absolute before:top-0 before:left-0 before:-z-10 before:h-full before:w-full ',
        className,
        styles.wrapper
      )}
      {...props}
    >
      <span className="after:btn-shape-smaller btn-shape-small absolute top-1/2 left-1/2 -z-20 h-[85%] w-[calc(100%+14px)] -translate-x-1/2 -translate-y-1/2 bg-primary after:absolute after:top-1/2 after:left-1/2 after:-z-20 after:h-[calc(100%)] after:w-[calc(100%)] after:-translate-x-1/2 after:-translate-y-1/2 after:bg-[#0C0532]"></span>
      {children}
    </BaseButton>
    // <BaseButton
    //   type="button"
    //   className={classNames(
    //     'relative py-3 font-TTNormsPro',
    //     className,
    //     styles.wrapper
    //   )}
    //   {...props}
    // >
    //   <span></span>
    //   {children}
    // </BaseButton>
  )
}

export default Button
