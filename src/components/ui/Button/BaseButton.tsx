import classNames from 'classnames'
import React from 'react'

export interface BaseButtonProps
  extends Omit<React.ButtonHTMLAttributes<HTMLButtonElement>, 'onClick'> {
  LeftIcon?: React.ComponentType<{ className: string }>
  iconClassName?: string
  primary?: boolean
  secondary?: boolean
  disabled?: boolean
  large?: boolean
  onClick?: (e: React.MouseEvent<HTMLButtonElement, MouseEvent> | null) => void
}

const BaseButton = React.forwardRef<HTMLButtonElement, BaseButtonProps>(
  (props, ref) => {
    const {
      className,
      iconClassName,
      LeftIcon,
      primary = false,
      secondary = false,
      disabled = false,
      large = false,
      children,
      onClick,
      ...rest
    } = props

    let buttonClassName

    if (disabled) {
      buttonClassName = 'bg-black/20 px-12'
    } else {
      if (primary) {
        if (large) {
          buttonClassName = ' px-[30px]'
        } else {
          buttonClassName = ' px-[30px]'
        }
      } else {
        buttonClassName = 'button-secondary-background px-9'
      }
    }

    return (
      <button
        type="button"
        className={classNames(
          'transition duration-300',
          className,
          buttonClassName
        )}
        onClick={(e) => {
          onClick?.(e)
        }}
        ref={ref}
        {...rest}
      >
        {LeftIcon && <LeftIcon className={iconClassName} />}
        {children}
      </button>
    )
  }
)

BaseButton.displayName = 'BaseButton'

export default BaseButton
