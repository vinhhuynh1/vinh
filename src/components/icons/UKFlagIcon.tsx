import React from 'react'
import Image from '../ui/Image'
import UKIcon from '../../../public/flags/UK.png'

interface UKFlagIconProps {
  className?: string
}

const UKFlagIcon: React.FC<UKFlagIconProps> = () => {
  return (
    <div className="relative xs:h-4 xs:w-[26px] lg:h-3 lg:w-5 2xl:h-4 2xl:w-[26px]">
      <Image src={UKIcon} alt="" layout="fill" />
    </div>
  )
}

export default UKFlagIcon
