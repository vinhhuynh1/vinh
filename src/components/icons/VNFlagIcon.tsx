import React from 'react'
import Image from '../ui/Image'
import VNIcon from '../../../public/flags/VN.png'

interface VNFlagIconProps {
  className?: string
}

const VNFlagIcon: React.FC<VNFlagIconProps> = () => {
  return (
    <div className="relative xs:h-4 xs:w-[26px] lg:h-3 lg:w-5 2xl:h-4 2xl:w-[26px]">
      <Image src={VNIcon} alt="" layout="fill" />
    </div>
  )
}

export default VNFlagIcon
