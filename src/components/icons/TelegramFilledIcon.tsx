import * as React from 'react'
import classNames from 'classnames'

function TelegramFilledIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M12 24C18.6274 24 24 18.6274 24 12C24 5.37258 18.6274 0 12 0C5.37258 0 0 5.37258 0 12C0 18.6274 5.37258 24 12 24Z"
        fill="url(#paint0_linear_5441_57736)"
      />
      <path
        d="M17.5814 7.74791C17.5486 8.24039 17.2613 9.96405 16.9822 11.8273L16.104 17.3512C16.104 17.3512 16.0301 18.1638 15.4391 18.3033C14.8482 18.4428 13.855 17.8108 13.6826 17.6713C13.5431 17.5646 11.0397 15.9805 10.1286 15.2089C9.88236 14.9955 9.60329 14.5769 10.1614 14.0844C11.4255 12.9189 12.9439 11.4825 13.855 10.5632C14.2736 10.1446 14.7004 9.15147 12.9357 10.3498L7.96991 13.6905C7.96991 13.6905 7.40356 14.0434 6.35295 13.7233C5.29412 13.4032 4.06293 12.9846 4.06293 12.9846C4.06293 12.9846 3.21752 12.4593 4.66211 11.8929L12.8618 8.51125C13.6744 8.15831 16.4159 7.03382 16.4159 7.03382C16.4159 7.03382 17.6799 6.54134 17.5814 7.7397V7.74791Z"
        fill="#110538"
      />
      <defs>
        <linearGradient
          id="paint0_linear_5441_57736"
          x1="-1.66192"
          y1="4.04346e-07"
          x2="29.5981"
          y2="23.0346"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00EBB0" />
          <stop offset="1" stopColor="#0481F2" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default TelegramFilledIcon
