import classNames from 'classnames'
import * as React from 'react'

function SwiperNext({ className, ...props }: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 90 90"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={classNames('', className)}
      {...props}
    >
      <rect
        x="0.859863"
        y="1.46069"
        width="87.5173"
        height="87.5757"
        rx="43.7587"
        fill="url(#paint0_radial_4467_40511)"
        stroke="url(#paint1_linear_4467_40511)"
        strokeWidth="1.25"
      />
      <path
        d="M36.2942 31.1965L62.647 48.0242L36.2942 64.6791V61.6464L58.486 48.0241L36.2942 34.4019V31.1965Z"
        fill="#1DE9B6"
      />
      <g filter="url(#filter0_d_4467_40511)">
        <path
          d="M52.9424 48.0244L36.2985 38.4088L36.2985 57.6401L52.9424 48.0244Z"
          fill="#1DE9B6"
        />
      </g>
      <defs>
        <filter
          id="filter0_d_4467_40511"
          x="26.2983"
          y="38.4087"
          width="36.644"
          height="39.2314"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="10" />
          <feGaussianBlur stdDeviation="5" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_4467_40511"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_4467_40511"
            result="shape"
          />
        </filter>
        <radialGradient
          id="paint0_radial_4467_40511"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="translate(46.0055 45.2485) rotate(-147.787) scale(44.2617 43.8679)"
        >
          <stop offset="0.0536038" stopColor="#2078AA" stopOpacity="0.7" />
          <stop offset="1" stopColor="#2078AA" stopOpacity="0.08" />
        </radialGradient>
        <linearGradient
          id="paint1_linear_4467_40511"
          x1="44.6185"
          y1="0.835693"
          x2="44.6185"
          y2="89.6614"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#1DE9B6" />
          <stop offset="0.202572" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="0.501401" stopColor="#04408F" />
          <stop offset="0.796586" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default SwiperNext
