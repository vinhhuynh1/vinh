import React from 'react'

const FacebookIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M16.0008 1C7.71561 1 1 7.71693 1 16C1 24.2831 7.71561 31 16.0008 31C24.2861 31 31 24.2848 31 16C31 7.71522 24.2844 1 16.0008 1ZM20.2795 10.4428C20.2795 10.4428 18.7031 10.4275 18.3068 10.4275C17.7099 10.4275 17.3137 10.9427 17.3137 11.4699V13.6652H20.2795L19.9411 16.6547H17.278V24.278H14.2663V16.6887H11.7205V13.6295H14.302V10.7455C14.302 9.13853 16.091 7.72203 17.3375 7.72203C17.5246 7.72203 20.2795 7.74923 20.2795 7.74923V10.4428V10.4428Z"
        fill="white"
      />
    </svg>
  )
}

export default FacebookIcon
