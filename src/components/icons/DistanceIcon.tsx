import React from 'react'

const DistanceIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 56 55"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle
        cx="28.1675"
        cy="27.2456"
        r="26.7456"
        fill="url(#paint0_radial_5441_57757)"
        stroke="url(#paint1_linear_5441_57757)"
      />
      <path
        d="M30.7842 34.9501C32.5543 35.4316 34.1173 35.7382 36.3631 36.2863C35.5003 44.9438 25.1919 41.5056 30.7842 34.9501ZM39.9879 26.9971C39.8717 24.3976 39.2951 20.9877 34.7029 21.522C32.5501 21.9682 30.9595 23.8458 30.2114 27.0852C29.8006 28.8664 30.0395 31.3665 30.5242 32.8651C30.9667 33.9213 30.8162 33.8563 31.2918 34.0569C33.1326 34.3862 34.955 34.7508 36.8109 35.1226C38.6961 34.0653 40.2445 28.4518 39.9879 26.9971ZM25.8158 26.7976C26.3001 25.2987 26.539 22.7986 26.1283 21.0177C25.3808 17.7781 23.79 15.9002 21.6369 15.4543C17.0447 14.92 16.4681 18.3299 16.3519 20.9295C16.0953 22.384 17.6438 27.9978 19.5293 29.0549C21.385 28.6831 23.2072 28.3189 25.0486 27.9892C25.5236 27.7889 25.3731 27.8537 25.8158 26.7976ZM19.9766 30.2189C20.839 38.8763 31.1473 35.4381 25.5553 28.8828C23.7851 29.3642 22.2222 29.6708 19.9766 30.2189Z"
        fill="#00F0FF"
      />
      <defs>
        <radialGradient
          id="paint0_radial_5441_57757"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="translate(29.0189 27.2456) rotate(-147.804) scale(27.1657 26.9163)"
        >
          <stop offset="0.0536038" stopColor="#1DE9B6" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" stopOpacity="0.1" />
        </radialGradient>
        <linearGradient
          id="paint1_linear_5441_57757"
          x1="28.1675"
          y1="0"
          x2="28.1675"
          y2="54.4913"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#1DE9B6" />
          <stop offset="0.202572" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="0.501401" stopColor="#04408F" />
          <stop offset="0.796586" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default DistanceIcon
