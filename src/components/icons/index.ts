import AppleIcon from './AppleIcon'
import ArrowRightIcon from './ArrowRightIcon'
import ArrowUpIcon from './ArrowUpIcon'
import CaloriesIcon from './CaloriesIcon'
import CarbonIcon from './CarbonIcon'
import ChevronDownIcon from './ChevronDownIcon'
import CHPlayIcon from './CHPlayIcon'
import CommunityIcon from './CommunityIcon'
import ContentIcon from './ContentIcon'
import DiscordIcon from './DiscordIcon'
import DiscordRoundedIcon from './DiscordRoundedIcon'
import DistanceIcon from './DistanceIcon'
import FacebookFilledIcon from './FacebookFilledIcon copy'
import FacebookIcon from './FacebookIcon'
import LinkIcon from './LinkIcon'
import MenuIcon from './MenuIcon'
import SearchIcon from './SearchIcon'
import SwiperNext from './SwiperNext'
import SwiperPrev from './SwiperPrev'
import TelegramFilledIcon from './TelegramFilledIcon'
import TelegramIcon from './TelegramIcon'
import TelegramRoundedIcon from './TelegramRoundedIcon'
import TelegramRoundedLineIcon from './TelegramRoundedLineIcon'
import TwitterFilledIcon from './TwitterFilledIcon'
import TwitterIcon from './TwitterIcon'
import TwitterRoundedLineIcon from './TwitterRoundedLineIcon'
import TwitterRoundedIcon from './TwitterRoundedIcon'
import UKFlagIcon from './UKFlagIcon'
import VNFlagIcon from './VNFlagIcon'

import * as LinkTreeIcon from './LinkTreeIcon'

export {
  AppleIcon,
  ArrowRightIcon,
  ArrowUpIcon,
  CaloriesIcon,
  CarbonIcon,
  ChevronDownIcon,
  CHPlayIcon,
  CommunityIcon,
  ContentIcon,
  DiscordIcon,
  DiscordRoundedIcon,
  DistanceIcon,
  FacebookFilledIcon,
  FacebookIcon,
  LinkIcon,
  MenuIcon,
  SearchIcon,
  SwiperNext,
  SwiperPrev,
  TelegramFilledIcon,
  TelegramIcon,
  TelegramRoundedIcon,
  TelegramRoundedLineIcon,
  TwitterFilledIcon,
  TwitterIcon,
  TwitterRoundedLineIcon,
  TwitterRoundedIcon,
  UKFlagIcon,
  VNFlagIcon,
  LinkTreeIcon,
}
