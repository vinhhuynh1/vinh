import * as React from 'react'
import classNames from 'classnames'

function FacebookFilledIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#clip0_5441_57727)">
        <path
          d="M24 12C24 5.3762 18.632 0 12 0C5.36799 0 0 5.3762 0 12C0 17.9918 4.39125 22.9576 10.1286 23.8523V15.4637H7.08345V11.9918H10.1286V9.34884C10.1286 6.34473 11.9179 4.67852 14.6594 4.67852C15.9726 4.67852 17.3434 4.91655 17.3434 4.91655V7.87141H15.8331C14.3393 7.87141 13.8796 8.79891 13.8796 9.74282V11.9918H17.212L16.6785 15.4637H13.8796V23.8523C19.617 22.9494 24.0082 17.9836 24.0082 12H24Z"
          fill="url(#paint0_linear_5441_57727)"
        />
        <path
          d="M16.6703 15.4727L17.2038 12.0008H13.8714V9.7518C13.8714 8.79968 14.3393 7.88039 15.8249 7.88039H17.3352V4.92553C17.3352 4.92553 15.9644 4.6875 14.6512 4.6875C11.9097 4.6875 10.1204 6.3455 10.1204 9.35781V12.0008H7.07526V15.4727H10.1204V23.8612C10.7278 23.9597 11.3598 24.009 11.9918 24.009C12.6238 24.009 13.2558 23.9597 13.8632 23.8612V15.4727H16.6621H16.6703Z"
          fill="#110538"
        />
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_5441_57727"
          x1="-1.66249"
          y1="4.01857e-07"
          x2="29.4645"
          y2="23.0866"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00EBB0" />
          <stop offset="1" stopColor="#0481F2" />
        </linearGradient>
        <clipPath id="clip0_5441_57727">
          <rect width="24" height="24" fill="white" />
        </clipPath>
      </defs>
    </svg>
  )
}

export default FacebookFilledIcon
