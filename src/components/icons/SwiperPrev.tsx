import classNames from 'classnames'
import * as React from 'react'

function SwiperPrev({ className, ...props }: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 90 90"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={classNames('', className)}
      {...props}
    >
      <rect
        x="-0.625"
        y="0.625"
        width="87.5173"
        height="87.5757"
        rx="43.7587"
        transform="matrix(-1 0 0 1 87.5171 0.835938)"
        fill="url(#paint0_radial_4467_40516)"
        stroke="url(#paint1_linear_4467_40516)"
        strokeWidth="1.25"
      />
      <path
        d="M52.7083 31.1963L26.3555 48.024L52.7083 64.6788V61.6461L30.5164 48.0239L52.7083 34.4017V31.1963Z"
        fill="#1DE9B6"
      />
      <g filter="url(#filter0_d_4467_40516)">
        <path
          d="M36.0601 48.0247L52.7039 38.409L52.7039 57.6404L36.0601 48.0247Z"
          fill="#1DE9B6"
        />
      </g>
      <defs>
        <filter
          id="filter0_d_4467_40516"
          x="26.0601"
          y="38.4089"
          width="36.644"
          height="39.2314"
          filterUnits="userSpaceOnUse"
          colorInterpolationFilters="sRGB"
        >
          <feFlood floodOpacity="0" result="BackgroundImageFix" />
          <feColorMatrix
            in="SourceAlpha"
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
            result="hardAlpha"
          />
          <feOffset dy="10" />
          <feGaussianBlur stdDeviation="5" />
          <feComposite in2="hardAlpha" operator="out" />
          <feColorMatrix
            type="matrix"
            values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.25 0"
          />
          <feBlend
            mode="normal"
            in2="BackgroundImageFix"
            result="effect1_dropShadow_4467_40516"
          />
          <feBlend
            mode="normal"
            in="SourceGraphic"
            in2="effect1_dropShadow_4467_40516"
            result="shape"
          />
        </filter>
        <radialGradient
          id="paint0_radial_4467_40516"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="translate(45.7706 44.4128) rotate(-147.787) scale(44.2617 43.8679)"
        >
          <stop offset="0.0536038" stopColor="#2078AA" stopOpacity="0.7" />
          <stop offset="1" stopColor="#2078AA" stopOpacity="0.08" />
        </radialGradient>
        <linearGradient
          id="paint1_linear_4467_40516"
          x1="44.3836"
          y1="0"
          x2="44.3836"
          y2="88.8257"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#1DE9B6" />
          <stop offset="0.202572" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="0.501401" stopColor="#04408F" />
          <stop offset="0.796586" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default SwiperPrev
