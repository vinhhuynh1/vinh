import React from 'react'

const CommunityIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="156"
      height="156"
      viewBox="0 0 156 156"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M153.748 78C153.748 119.977 119.775 154 77.874 154C35.973 154 2 119.977 2 78C2 36.0233 35.973 2 77.874 2C119.775 2 153.748 36.0233 153.748 78Z"
        stroke="url(#paint0_linear_4555_31181)"
        strokeWidth="4"
      />
      <g clipPath="url(#clip0_4555_31181)">
        <path
          d="M55.3 70.2427C59.9749 70.2427 63.7646 66.1578 63.7646 61.1188C63.7646 56.0798 59.9749 51.9949 55.3 51.9949C50.6252 51.9949 46.8354 56.0798 46.8354 61.1188C46.8354 66.1578 50.6252 70.2427 55.3 70.2427Z"
          fill="url(#paint1_linear_4555_31181)"
        />
        <path
          d="M64.5826 76.5381C61.7427 78.4483 59.4005 81.106 57.7766 84.2611C56.1526 87.4162 55.3002 90.9648 55.2998 94.573H49.6567C48.1615 94.5681 46.7287 93.9258 45.6714 92.7861C44.6141 91.6464 44.0181 90.1021 44.0137 88.4904V82.4078C44.0204 79.9902 44.9143 77.6737 46.5003 75.9642C48.0862 74.2547 50.2354 73.2912 52.4782 73.284H58.1213C59.3531 73.2869 60.5694 73.5797 61.6854 74.1417C62.8014 74.7038 63.7901 75.5216 64.5826 76.5381Z"
          fill="url(#paint2_linear_4555_31181)"
        />
        <path
          d="M100.444 70.2427C105.118 70.2427 108.908 66.1578 108.908 61.1188C108.908 56.0798 105.118 51.9949 100.444 51.9949C95.7687 51.9949 91.979 56.0798 91.979 61.1188C91.979 66.1578 95.7687 70.2427 100.444 70.2427Z"
          fill="url(#paint3_linear_4555_31181)"
        />
        <path
          d="M111.73 82.4078V88.4903C111.725 90.1021 111.129 91.6464 110.072 92.786C109.014 93.9257 107.582 94.5681 106.086 94.5729H100.443C100.443 90.9648 99.5906 87.4161 97.9667 84.261C96.3427 81.1059 94.0005 78.4482 91.1606 76.5381C91.9531 75.5216 92.9418 74.7038 94.0578 74.1417C95.1738 73.5796 96.3901 73.2869 97.6219 73.2839H103.265C105.508 73.2911 107.657 74.2547 109.243 75.9642C110.829 77.6737 111.723 79.9902 111.73 82.4078Z"
          fill="url(#paint4_linear_4555_31181)"
        />
        <path
          d="M77.8715 76.3252C84.1047 76.3252 89.1576 70.8787 89.1576 64.1601C89.1576 57.4415 84.1047 51.9949 77.8715 51.9949C71.6384 51.9949 66.5854 57.4415 66.5854 64.1601C66.5854 70.8787 71.6384 76.3252 77.8715 76.3252Z"
          fill="url(#paint5_linear_4555_31181)"
        />
        <path
          d="M94.8002 94.5728V97.6141C94.7935 100.032 93.8995 102.348 92.3135 104.058C90.7276 105.767 88.5785 106.731 86.3356 106.738H69.4065C67.1636 106.731 65.0145 105.767 63.4285 104.058C61.8425 102.348 60.9486 100.032 60.9419 97.6141V94.5728C60.9419 90.5398 62.4282 86.672 65.0739 83.8203C67.7196 80.9685 71.3079 79.3664 75.0495 79.3664H80.6925C84.4341 79.3664 88.0224 80.9685 90.6681 83.8203C93.3138 86.672 94.8002 90.5398 94.8002 94.5728Z"
          fill="url(#paint6_linear_4555_31181)"
        />
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_4555_31181"
          x1="80.0804"
          y1="153.79"
          x2="172.745"
          y2="54.2874"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0185FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_4555_31181"
          x1="55.5398"
          y1="69.9842"
          x2="66.4065"
          y2="59.1412"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_4555_31181"
          x1="54.5895"
          y1="94.2714"
          x2="67.2579"
          y2="81.1069"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_4555_31181"
          x1="100.683"
          y1="69.9842"
          x2="111.55"
          y2="59.1412"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint4_linear_4555_31181"
          x1="101.736"
          y1="94.2713"
          x2="114.405"
          y2="81.1069"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint5_linear_4555_31181"
          x1="78.1913"
          y1="75.9806"
          x2="92.6801"
          y2="61.5233"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <linearGradient
          id="paint6_linear_4555_31181"
          x1="78.3507"
          y1="106.35"
          x2="94.0082"
          y2="85.5191"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#0085FF" />
          <stop offset="1" stopColor="#00FFBF" />
        </linearGradient>
        <clipPath id="clip0_4555_31181">
          <rect
            width="67.7165"
            height="72.9908"
            fill="white"
            transform="translate(44.0137 42.8711)"
          />
        </clipPath>
      </defs>
    </svg>
  )
}

export default CommunityIcon
