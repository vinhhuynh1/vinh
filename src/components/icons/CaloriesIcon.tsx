import React from 'react'

const CaloriesIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="100%"
      height="100%"
      viewBox="0 0 56 55"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <circle
        cx="28.1675"
        cy="27.2456"
        r="26.7456"
        fill="url(#paint0_radial_5441_57742)"
        stroke="url(#paint1_linear_5441_57742)"
      />
      <path
        d="M38.675 31.8948C38.6534 34.474 37.6093 36.9392 35.7719 38.7492C33.9345 40.5593 31.4539 41.5663 28.8748 41.5493C26.2944 41.5682 23.8121 40.5619 21.9732 38.7516C20.1344 36.9414 19.0893 34.4752 19.0677 31.8948C19.0677 27.6615 21.7252 24.0356 21.7374 20.3171C22.8211 21.3455 23.7638 22.513 24.5406 23.7891C26.2132 19.8785 28.9469 18.5791 30.9833 14.5664C33.1297 17.7611 34.4152 21.4557 34.7154 25.2928C35.001 24.4434 35.4533 23.6595 36.0458 22.9871C36.6382 22.3146 37.3589 21.7672 38.1655 21.3768C37.6357 25.5053 38.675 28.8751 38.675 31.8948Z"
        fill="#00F0FF"
      />
      <path
        d="M33.4896 37.0003C33.4896 38.2253 33.003 39.4001 32.1368 40.2663C31.2706 41.1325 30.0958 41.6191 28.8708 41.6191C27.6458 41.6191 26.471 41.1325 25.6048 40.2663C24.7386 39.4001 24.252 38.2253 24.252 37.0003C24.252 35.0062 25.407 32.9903 25.4138 31.2373C26.2562 31.7115 26.913 32.4571 27.2771 33.3526C27.8492 30.8913 28.958 30.909 29.8665 28.8359C30.8833 30.3764 31.4234 32.1824 31.4192 34.0282C31.8331 33.0875 32.5672 32.3239 33.491 31.8734C33.3381 33.5789 33.3381 35.2947 33.491 37.0003H33.4896Z"
        fill="#00C1CD"
      />
      <defs>
        <radialGradient
          id="paint0_radial_5441_57742"
          cx="0"
          cy="0"
          r="1"
          gradientUnits="userSpaceOnUse"
          gradientTransform="translate(29.0189 27.2456) rotate(-147.804) scale(27.1657 26.9163)"
        >
          <stop offset="0.0536038" stopColor="#1DE9B6" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" stopOpacity="0.1" />
        </radialGradient>
        <linearGradient
          id="paint1_linear_5441_57742"
          x1="28.1675"
          y1="0"
          x2="28.1675"
          y2="54.4913"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#1DE9B6" />
          <stop offset="0.202572" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="0.501401" stopColor="#04408F" />
          <stop offset="0.796586" stopColor="#04408F" stopOpacity="0.5" />
          <stop offset="1" stopColor="#1DE9B6" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default CaloriesIcon
