import React from 'react'

const MenuIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="24"
      height="18"
      viewBox="0 0 24 18"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <line
        x1="1"
        y1="1"
        x2="23"
        y2="1"
        stroke="#00F0FF"
        strokeWidth="2"
        strokeLinecap="square"
      />
      <line
        x1="13"
        y1="9"
        x2="23"
        y2="9"
        stroke="#00F0FF"
        strokeWidth="2"
        strokeLinecap="square"
      />
      <line
        opacity="0.5"
        x1="1"
        y1="9"
        x2="7"
        y2="9"
        stroke="#00F0FF"
        strokeWidth="2"
        strokeLinecap="square"
      />
      <line
        x1="1"
        y1="17"
        x2="23"
        y2="17"
        stroke="#00F0FF"
        strokeWidth="2"
        strokeLinecap="square"
      />
    </svg>
  )
}

export default MenuIcon
