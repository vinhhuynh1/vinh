import classNames from 'classnames'
import React from 'react'

const CHPlayIcon = (props: React.SVGProps<SVGSVGElement>) => {
  return (
    <svg
      width="32"
      height="32"
      viewBox="0 0 32 32"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      className={classNames('', props.className)}
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M5.45983 3.39847C5.16783 3.71259 5 4.19607 5 4.82432V27.171C5 27.7992 5.16783 28.2827 5.47075 28.5859L5.54989 28.6528L18.0595 16.1315V15.8501L5.53897 3.33154L5.45983 3.39847Z"
        fill="url(#paint0_linear_1907_28055)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22.2195 20.3223L18.0469 16.1445V15.8522L22.2208 11.6743L22.3109 11.7303L27.2476 14.5383C28.6612 15.3359 28.6612 16.6498 27.2476 17.4583L22.3109 20.2663C22.3095 20.2663 22.2195 20.3223 22.2195 20.3223Z"
        fill="url(#paint1_linear_1907_28055)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22.3124 20.2661L18.0497 15.998L5.46094 28.5986C5.92077 29.093 6.69443 29.149 7.55952 28.6655L22.3124 20.2661Z"
        fill="url(#paint2_linear_1907_28055)"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22.3124 11.7316L7.55952 3.34307C6.6958 2.84866 5.92077 2.91558 5.46094 3.40999L18.0483 15.9982L22.3124 11.7316Z"
        fill="url(#paint3_linear_1907_28055)"
      />
      <path
        opacity="0.2"
        fillRule="evenodd"
        clipRule="evenodd"
        d="M22.2231 20.176L7.57124 28.5086C6.75255 28.9797 6.02254 28.947 5.5518 28.5195L5.47266 28.5987L5.5518 28.6656C6.02254 29.0917 6.75255 29.1259 7.57124 28.6547L22.3241 20.2662L22.2231 20.176Z"
        fill="black"
      />
      <path
        opacity="0.12"
        fillRule="evenodd"
        clipRule="evenodd"
        d="M27.2486 17.3119L22.2109 20.1759L22.301 20.2661L27.2377 17.458C27.9445 17.0538 28.2925 16.5266 28.2925 15.998C28.2474 16.4815 27.8886 16.9418 27.2486 17.3119Z"
        fill="black"
      />
      <path
        opacity="0.25"
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.55841 3.48883L27.248 14.6854C27.8879 15.0446 28.2468 15.5158 28.3027 15.9992C28.3027 15.472 27.9548 14.9435 27.248 14.5392L7.55841 3.34269C6.1448 2.53416 5 3.20748 5 4.82455V4.97069C5 3.35362 6.1448 2.69122 7.55841 3.48883Z"
        fill="white"
      />
      <defs>
        <linearGradient
          id="paint0_linear_1907_28055"
          x1="16.9362"
          y1="4.58412"
          x2="-3.10453"
          y2="9.919"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#00A0FF" />
          <stop offset="0.007" stopColor="#00A1FF" />
          <stop offset="0.26" stopColor="#00BEFF" />
          <stop offset="0.512" stopColor="#00D2FF" />
          <stop offset="0.76" stopColor="#00DFFF" />
          <stop offset="1" stopColor="#00E3FF" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_1907_28055"
          x1="29.0871"
          y1="15.9989"
          x2="4.65472"
          y2="15.9989"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FFE000" />
          <stop offset="0.409" stopColor="#FFBD00" />
          <stop offset="0.775" stopColor="#FFA500" />
          <stop offset="1" stopColor="#FF9C00" />
        </linearGradient>
        <linearGradient
          id="paint2_linear_1907_28055"
          x1="19.9945"
          y1="18.3199"
          x2="3.8854"
          y2="45.4071"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#FF3A44" />
          <stop offset="1" stopColor="#C31162" />
        </linearGradient>
        <linearGradient
          id="paint3_linear_1907_28055"
          x1="2.29204"
          y1="-4.03453"
          x2="9.47878"
          y2="8.06306"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#32A071" />
          <stop offset="0.069" stopColor="#2DA771" />
          <stop offset="0.476" stopColor="#15CF74" />
          <stop offset="0.801" stopColor="#06E775" />
          <stop offset="1" stopColor="#00F076" />
        </linearGradient>
      </defs>
    </svg>
  )
}

export default CHPlayIcon
