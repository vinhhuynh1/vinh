import React, { PropsWithChildren } from 'react'
import Footer from '../common/Footer/Footer'
import Header from '../common/Header'

interface BaseLayoutProps {}

const BaseLayout: React.FC<PropsWithChildren<BaseLayoutProps>> = ({
  children,
}) => {
  return (
    <>
      <Header />
      <main>{children}</main>
      <Footer />
    </>
  )
}

export default BaseLayout
