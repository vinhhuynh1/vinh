import LanguageSwitcher from '@/components/features/LanguageSwitcher'
import classNames from 'classnames'
import { useRouter } from 'next/router'
import React from 'react'
import { Button } from '../../ui/Button'
import Logo from '../../ui/Logo'
import { NavItem } from '../../ui/Nav'
import { useTranslation } from 'next-i18next'
import { useWindowDimensions } from '@/hooks'
import { Else, If, Then } from 'react-if'
import Drawer from '../Drawer'
import { DrawerRef } from '../Drawer/Drawer'

const routes = [
  {
    title: 'home',
    href: '/',
  },
  {
    title: 'feature',
    href: '/feature',
  },
  {
    title: 'litepaper',
    href: '/litepaper',
  },
  {
    title: 'linktree',
    href: '/linktree',
  },
  {
    title: 'marketplace',
    href: '/marketplace',
  },
]

const Header = () => {
  const router = useRouter()
  const { width } = useWindowDimensions()
  const { t } = useTranslation('header')

  const drawerRef = React.useRef<DrawerRef>(null)

  return (
    <header className="flex border-[rgba(0,240,255,0.5)] xs:h-14 lg:h-[110px] lg:border-b">
      <div className="container flex items-center xs:justify-between">
        <div className="">
          <NavItem href="/">{() => <Logo />}</NavItem>
        </div>
        <If condition={width >= 1024}>
          <Then>
            <nav className="ml-auto hidden items-center lg:flex lg:space-x-[1.375rem] 2xl:space-x-10">
              {routes.map((route) => (
                <NavItem
                  href={route.href}
                  key={route.href}
                  className="relative group"
                >
                  {({ isActive }) => (
                    <span
                      className={classNames(
                        'font-TTNormsPro text-base capitalize transition duration-300',
                        isActive && 'font-bold text-primary',
                        !isActive && 'font-normal text-[#bcbcbc]'
                      )}
                    >
                      {t(route.title)}
                    </span>
                  )}
                </NavItem>
              ))}
            </nav>
            <div className="ml-10">
              <LanguageSwitcher />
            </div>
            <Button
              primary
              className="hidden items-center font-medium lg:ml-6 lg:flex lg:text-xs lg:leading-[18px] 2xl:ml-[49px] 2xl:text-[14px] 2xl:leading-[28px]"
              onClick={() => router.push('/whitepaper')}
            >
              Whitepaper
            </Button>
          </Then>
          <Else>
            <Drawer ref={drawerRef}>
              <nav className="flex flex-col items-center space-y-10">
                {routes.map((route) => (
                  <div onClick={drawerRef.current?.close} key={route.href}>
                    <NavItem href={route.href} className="relative group">
                      {({ isActive }) => (
                        <span
                          className={classNames(
                            'font-TTNormsPro text-[32px] capitalize leading-[54px] transition duration-300',
                            isActive && 'font-bold text-primary',
                            !isActive && 'font-normal text-[#bcbcbc]'
                          )}
                        >
                          {t(route.title)}
                        </span>
                      )}
                    </NavItem>
                  </div>
                ))}
              </nav>
            </Drawer>
          </Else>
        </If>
      </div>
    </header>
  )
}

export default Header
