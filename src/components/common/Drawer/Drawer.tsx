import LanguageSwitcher from '@/components/features/LanguageSwitcher'
import { MenuIcon } from '@/components/icons'
import classNames from 'classnames'
import { motion, AnimatePresence, Variants, Transition } from 'framer-motion'
import React, { useEffect } from 'react'
import Portal from '../Portal'

interface DrawerProps {
  containerClassName?: string
  children?: React.ReactNode
}

export type DrawerRef = {
  close: () => void
  open: () => void
}

const drawerVariants: Variants = {
  initial: {
    x: '100%',
  },

  enter: {
    x: 0,
  },

  exit: {
    x: '-100%',
  },
}

const transition: Transition = {
  ease: 'easeInOut',
}

const Drawer = React.forwardRef<DrawerRef, DrawerProps>((props, ref) => {
  const { containerClassName, children } = props
  const [isOpen, setIsOpen] = React.useState<boolean>(false)

  const handleClose = () => {
    setIsOpen(false)
  }
  const handleOpen = () => setIsOpen(true)

  React.useImperativeHandle(
    ref,
    () => ({
      close: handleClose,
      open: handleOpen,
    }),
    []
  )

  return (
    <div className={classNames(containerClassName)}>
      <div className="flex items-center">
        <div>
          <LanguageSwitcher />
        </div>
        <div className="ml-[30px]">
          <MenuIcon onClick={handleOpen} />
        </div>
      </div>
      <Portal>
        <AnimatePresence>
          {isOpen && (
            <motion.div
              className="fixed inset-0 top-0 left-0 z-[1000] flex flex-col items-center justify-center bg-[rgba(0,9,65,0.75)] backdrop-blur-md"
              variants={drawerVariants}
              initial="initial"
              animate="enter"
              exit="exit"
              transition={transition}
            >
              {children}
            </motion.div>
          )}
        </AnimatePresence>
      </Portal>
    </div>
  )
})

Drawer.displayName = 'Drawer'

export default Drawer
