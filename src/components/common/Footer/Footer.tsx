import ArrowRightIcon from '@/components/icons/ArrowRightIcon'
import DiscordIcon from '@/components/icons/DiscordIcon'
import FacebookIcon from '@/components/icons/FacebookIcon'
import LinkIcon from '@/components/icons/LinkIcon'
import TelegramIcon from '@/components/icons/TelegramRoundedLineIcon'
import TwitterRoundedLineIcon from '@/components/icons/TwitterRoundedLineIcon'
import Link from 'next/link'
import React from 'react'
import Logo from '../../ui/Logo'

const links = [
  {
    title: 'Home',
    href: '/',
  },
  {
    title: 'Feature',
    href: '/howtoplay',
  },
  {
    title: 'Litepaper',
    href: '/litepaper',
  },
  {
    title: 'Linktree',
    href: '/linktree',
  },
  {
    title: 'Marketplace',
    href: '/#!',
  },
  {
    title: 'Whitepaper',
    href: '/whitepaper',
  },
]

const socials = [
  {
    title: 'Discord',
    href: '/#!',
  },
  {
    title: 'Telegram',
    href: '/#!',
  },
  {
    title: 'Twitter',
    href: '/#!',
  },
  {
    title: 'Facebook',
    href: '/#!',
  },
  {
    title: 'Medium',
    href: '/#!',
  },
]

const contacts = [
  {
    icon: <TwitterRoundedLineIcon />,
    href: '/#!',
  },
  {
    icon: <DiscordIcon />,
    href: '/#!',
  },
  {
    icon: <TelegramIcon />,
    href: '/#!',
  },
  {
    icon: <LinkIcon />,
    href: '/#!',
  },
]

const Footer = () => {
  return (
    <footer className="pb-6 xs:pt-14 lg:pt-24">
      <div className="container flex justify-between mb-16 xs:flex-col xs:space-y-8 lg:flex-row lg:space-y-0">
        <div className="relative">
          <Logo />
          <p className="mt-6 italic xs:text-xs xs:leading-5 lg:max-w-[227px] 2xl:max-w-[295px] 2xl:text-body-5">
            PGO aims to be an exclusive concept of the wealthy through health, a
            reliable app for fitness enthusiasts looking to boost their income
            with NFT rewards.
          </p>
        </div>
        <div className="">
          <ColumnHeading title="Quick link" />
          <ul className="flex xs:flex-row xs:flex-wrap lg:flex-col lg:flex-nowrap lg:space-y-1 2xl:space-y-3">
            {links.map((link) => (
              <li className="mt-2 xs:w-1/4 lg:w-auto" key={link.href}>
                <Link href={link.href}>
                  <a className="italic lg:text-xs lg:leading-[25px] 2xl:text-body-5">
                    {link.title}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="">
          <ColumnHeading title="Follow us" />
          <ul className="flex xs:flex-row xs:flex-wrap lg:flex-col lg:flex-nowrap lg:space-y-1 2xl:space-y-3">
            {socials.map((link, idx) => (
              <li className="mt-2 xs:w-1/4 lg:w-auto" key={idx}>
                <Link href={link.href}>
                  <a className="italic lg:text-xs lg:leading-[25px] 2xl:text-body-5">
                    {link.title}
                  </a>
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="lg:max-w-[284px] 2xl:max-w-[368px]">
          <ColumnHeading title="Join New Letters" />
          <p className="italic xs:text-[14px] xs:leading-[25px] 2xl:text-body-5">
            Be the first to know about platform updates & exclusive promotions.
          </p>
          <form className="relative mt-4">
            <input
              type="email"
              placeholder="Enter Your Mail"
              spellCheck={false}
              className="w-full border border-[#02C2A0] bg-[rgba(22,23,50,0.51)] text-body-6 italic text-white placeholder-white xs:py-[13px] xs:pl-4 2xl:py-[17px] 2xl:pl-[18px]"
            />
            <button
              type="submit"
              className="text-gradient absolute right-0 top-0 flex h-full w-[55px] items-center justify-center"
            >
              <ArrowRightIcon />
            </button>
          </form>
          <ul className="flex mt-6 space-x-8">
            {contacts.map((contact, idx) => {
              return (
                <li
                  key={idx}
                  className="group flex cursor-pointer items-center justify-center xs:h-[30px] xs:w-[30px] 2xl:h-10 2xl:w-10"
                >
                  <Link href={contact.href}>
                    <a className="">{contact.icon}</a>
                  </Link>
                </li>
              )
            })}
          </ul>
        </div>
      </div>
      {/* <hr className="bg-ruler-gradient mx-auto h-[1px] max-w-[1205px] border-0" /> */}
      <div className="border-t border-[rgba(0,240,255,0.5)] pt-[22px]">
        <div className="container relative flex flex-col-reverse items-center lg:flex-row lg:justify-between 2xl:justify-end">
          <p className="mt-4 italic text-[#bcbcbc] lg:static lg:mt-0 2xl:absolute 2xl:left-1/2 2xl:-translate-x-1/2">
            Copyright © P-Go2022
          </p>

          <div className="flex justify-end space-x-10">
            <Link href="#!">
              <a className="italic font-bold">Terms of Service</a>
            </Link>
            <Link href="#!">
              <a className="ml-[138px] font-bold italic">Policy</a>
            </Link>
            <Link href="#!">
              <a className="ml-[138px] font-bold italic">Report a Bug</a>
            </Link>
          </div>
        </div>
      </div>
    </footer>
  )
}

const ColumnHeading = (props: { title: string }) => {
  return (
    <div className="text-gradient bg-clip-text font-medium italic text-transparent xs:mb-2 xs:text-[14px] xs:leading-[29px] 2xl:mb-5 2xl:text-body-4">
      {props.title}
    </div>
  )
}

export default Footer
