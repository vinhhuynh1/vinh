import { UKFlagIcon } from '@/components/icons'
import SelectOptions from '@/components/ui/SelectOptions'
import { OptionsProps } from '@/components/ui/SelectOptions/SelectOptions'
import { useRouter } from 'next/router'
import React from 'react'
import nookies from 'nookies'
import { components, SingleValue, StylesConfig } from 'react-select'
import VNFlagIcon from '@/components/icons/VNFlagIcon'

interface LanguageSwitcherProps {}

const locales = [
  {
    label: <UKFlagIcon />,
    value: 'en',
    name: 'English',
  },
  // {
  //   label: <UKFlagIcon />,
  //   value: 'zh',
  //   name: 'Chinese',
  // },
  {
    label: <VNFlagIcon />,
    value: 'vi',
    name: 'Vietnamese',
  },
  // {
  //   label: <UKFlagIcon />,
  //   value: 'es',
  //   name: 'Spanish',
  // },
]

const LanguageSwitcher: React.FC<LanguageSwitcherProps> = () => {
  const router = useRouter()
  const [selectOption, setSelectOption] = React.useState(() => {
    return locales.find(({ value }) => router.locale === value)
  })

  const customStyle: StylesConfig<OptionsProps, false> = {
    container: (provided) => ({
      ...provided,
      // width: 50,
      cursor: 'pointer',
    }),
    control: (provided) => ({
      ...provided,
      // padding: '11px 16px',
      borderRadius: '0',
      backgroundColor: 'transparent',
      border: 0,
      cursor: 'pointer',
      outline: 'none',
      boxShadow: 'none',
      minHeight: 'auto',
      // minWidth: '80px',
    }),
    valueContainer: (provided) => ({
      ...provided,
      padding: '0',
      placeItems: 'center',
      height: '100%',
      width: '100%',
      minWidth: '26px',

      '& input:nth-of-type(1)': {
        position: 'absolute',
      },
    }),
    singleValue: (provided) => ({
      ...provided,
      textAlign: 'center',
      fontSize: '18px',
      color: '#ffffff',
      lineHeight: '22px',
      fontWeight: 'bold',
      textTransform: 'uppercase',
      // display: 'inline-block',
      margin: '0',
      display: 'inline-flex',
      alignItems: 'center',
      justifyContent: 'center',
      width: '100%',
    }),
    menu: (provided, state) => ({
      ...provided,
      // padding: 0,
      right: '8px',
      borderRadius: '0',
      backgroundColor: '#09072F',
      width: 'auto',
      minWidth: '191px',
      border: '1px solid #1DE9B6',

      '@media only screen and (max-width: 1023px)': {
        minWidth: '162px',
      },
    }),
    menuList: (provided, state) => ({
      ...provided,
      padding: '12px 8px',
      zIndex: 1000,
      // borderRadius: '0',
    }),
    option: (provided, state) => ({
      // provided has CSSObject type
      // state has ControlProps type

      // return type is CSSObject which means this line will throw error if uncommented
      // return false;

      ...provided,
      color: '#FFFFFF',
      backgroundColor: state.isSelected ? '#1A2756' : 'transparent',
      padding: '4px 8px',

      textAlign: 'center',
      fontSize: '18px',
      lineHeight: '22px',
      fontFamily: '"TT Norms Pro", sans-serif',
      fontStyle: 'italic',
      fontWeight: 'normal',

      cursor: 'pointer',
      zIndex: 1000,

      display: 'flex',
      alignItems: 'center',
    }),

    dropdownIndicator: (provided, state) => ({
      ...provided,
      color: '#DEDEDE',
      padding: 4,
    }),
  }

  const { Option } = components

  const IconOption = (props) => (
    <Option {...props}>
      {/* <img
        src={require('./' + props.data.icon)}
        style={{ width: 36 }}
        alt={props.data.label}
      /> */}
      {props.data.label}
      <span className="ml-[14px] text-xs leading-5">{props.data.name}</span>
    </Option>
  )

  const handleOptionChange = (option: SingleValue<OptionsProps>) => {
    if (router.locale === option?.value) return
    const { pathname, asPath, query } = router
    router.replace({ pathname, query }, asPath, {
      locale: option?.value,
      shallow: true,
    })
    nookies.set(null, 'NEXT_LOCALE', option?.value as string, { path: '/' })
  }
  return (
    <SelectOptions
      id="select-languages"
      instanceId="select-languages"
      options={locales}
      defaultValue={selectOption}
      customStyles={customStyle}
      onChange={handleOptionChange}
      isSearchable={false}
      components={{
        IndicatorSeparator: () => null,
        Option: IconOption,
      }}
      className="react-select-container"
      classNamePrefix="react-select"
    />
  )
}

export default LanguageSwitcher
