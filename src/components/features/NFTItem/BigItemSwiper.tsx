import Image from '@/components/ui/Image'
import Swiper from '@/components/ui/Swiper'
import classNames from 'classnames'
import { motion } from 'framer-motion'
import React from 'react'
import { SwiperProps, SwiperSlide } from 'swiper/react'

interface BigItemSwiperProps extends SwiperProps {
  data: any[]
}

const BigItemSwiper = ({ data, ...props }) => {
  return (
    <Swiper
      centeredSlides
      // centerInsufficientSlides
      loop
      hidePagination
      slidesPerView={1}
      loopedSlides={6}
      slidesPerGroup={1}
      spaceBetween={1}
      isOverflowHidden
      {...props}
    >
      {data &&
        data.map((item) => (
          <SwiperSlide key={item.name}>
            {({ isActive }) => {
              return (
                <motion.div
                  className="flex items-center justify-center"
                  // variants={{
                  //   initial: {
                  //     opacity: 0,
                  //   },
                  //   active: {
                  //     opacity: 1,
                  //     transition: {
                  //       duration: 2,
                  //     },
                  //   },
                  // }}
                  // animate={isActive ? 'active' : 'initial'}
                >
                  <Image
                    src={item.image}
                    alt={item.name}
                    width="730px"
                    height="573px"
                  />
                </motion.div>
              )
            }}
          </SwiperSlide>
        ))}
    </Swiper>
  )
}

export default BigItemSwiper
