import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import classNames from 'classnames'
import { motion } from 'framer-motion'
import React from 'react'
import BigItemSwiper from './BigItemSwiper'
import SmallItemSwiper from './SmallItemSwiper'

interface NFTItemsProps {}

const smallNFTItems = [
  {
    name: 'NFT 1',
    image: '/items/item-1.png',
  },
  {
    name: 'NFT 2',
    image: '/items/item-2.png',
  },
  {
    name: 'NFT 3',
    image: '/items/item-3.png',
  },
  {
    name: 'NFT 4',
    image: '/items/item-4.png',
  },
  {
    name: 'NFT 5',
    image: '/items/item-5.png',
  },
  {
    name: 'NFT 6',
    image: '/items/item-3.png',
  },
]

const bigNFTItems = [
  {
    name: 'NFT 1',
    image: '/items/big-1.png',
  },
  {
    name: 'NFT 2',
    image: '/items/big-1.png',
  },
  {
    name: 'NFT 3',
    image: '/items/big-1.png',
  },
  {
    name: 'NFT 4',
    image: '/items/big-1.png',
  },
  {
    name: 'NFT 5',
    image: '/items/big-1.png',
  },
  {
    name: 'NFT 6',
    image: '/items/big-1.png',
  },
]

const NFTItems: React.FC<NFTItemsProps> = () => {
  const [firstSwiper, setFirstSwiper] = React.useState(null)
  const [secondSwiper, setSecondSwiper] = React.useState(null)
  const [hasWindow, setHasWindow] = React.useState<boolean>(false)

  React.useEffect(() => {
    if (typeof window !== 'undefined') {
      setHasWindow(true)
    }
  }, [])

  return (
    <Section hasPadding className="mt-[140px]">
      <SectionHeading
        title="Items NFT"
        desc="With exclusive Sneaker NFT, you can use tokens to upgrade your sneaker, boost energy & Earn more for every calories burn."
      />

      <div className="relative mx-auto mt-28 xs:h-[221px] xs:w-[292px] lg:h-[398px] lg:w-[558px] 2xl:h-[605px] 2xl:w-[849px]">
        <Image src="/shoe-award.png" alt="shoe" layout="fill" />

        {/* Transfer */}
        <ItemInfo
          containerClassName="absolute xs:-top-[10px] xs:left-[14px] lg:-top-[9px] lg:left-10 2xl:-top-[25px] 2xl:left-[54px]"
          title="Transfer"
          image="/NFTItems/icons/transfer.png"
        />

        {/* Gem socket */}
        <ItemInfo
          containerClassName="absolute xs:top-[46px] xs:-left-[48px] lg:top-[90px] lg:-left-[56px] 2xl:top-[123px] 2xl:-left-[104px]"
          title="NFT Gem Socket"
          image="/NFTItems/icons/gem-socket.png"
        />

        {/* Gem socket */}
        <ItemInfo
          containerClassName="absolute xs:top-[115px] xs:-left-1 lg:top-[216px] lg:-left-3 2xl:top-[317px] 2xl:-left-4 -translate-x-full"
          title="In-app Sell"
          image="/NFTItems/icons/sell.png"
        />

        {/* Upgrade */}
        <ItemInfo
          containerClassName="absolute xs:-top-[26px] xs:right-[14px] lg:-top-[43px] lg:right-10 2xl:-top-[74px] 2xl:right-[54px]"
          title="Upgrade"
          image="/NFTItems/icons/upgrade.png"
        />

        {/* Repair */}
        <ItemInfo
          containerClassName="absolute xs:top-[30px] xs:-right-6 lg:top-[59px] lg:-right-[34px] 2xl:top-[86px] 2xl:-right-[58px]"
          title="Repair"
          image="/NFTItems/icons/repair.png"
        />

        {/* Attributes */}
        <ItemInfo
          containerClassName="absolute xs:top-[113px] xs:-right-1 lg:top-[212px] lg:-right-3 2xl:top-[314px] 2xl:-right-4 translate-x-full"
          title="Attributes"
          image="/NFTItems/icons/attributes.png"
        />

        <div className="bg-gradient-blue absolute top-1/2 left-1/2 z-[-1] h-[110%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
      </div>

      {/* <div className="mt-12">
        <div className="relative">
          {hasWindow && (
            <BigItemSwiper
              data={bigNFTItems}
              onSwiper={setSecondSwiper}
              controller={{ control: firstSwiper }}
            />
          )}
          <div className="absolute bottom-0 left-1/2 h-[231px] w-[750px] -translate-x-1/2 translate-y-1/3">
            <Image src="/award.png" alt="" layout="fill" objectFit="contain" />
          </div>
        </div>
      </div>
      <div className="mt-28">
        <div className="overflow-x-hidden">
          {hasWindow && (
            <SmallItemSwiper
              data={smallNFTItems}
              onSwiper={setFirstSwiper}
              controller={{ control: secondSwiper }}
            />
          )}
        </div>
      </div> */}
    </Section>
  )
}

const ItemInfo: React.FC<{
  image: string
  title: string
  containerClassName?: string
}> = ({ image, title, containerClassName }) => {
  return (
    <div
      className={classNames(containerClassName, 'flex flex-col items-center')}
    >
      <div className="relative xs:h-5 xs:w-5 lg:h-8 lg:w-8 2xl:h-14 2xl:w-14">
        <Image src={image} alt={title} layout="fill" />
      </div>
      <div className="capitalize italic text-primary xs:text-[10px] xs:leading-4 lg:text-sm lg:leading-6 2xl:text-2xl 2xl:leading-10">
        {title}
      </div>
    </div>
  )
}

export default NFTItems
