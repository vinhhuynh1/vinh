import Image from '@/components/ui/Image'
import Swiper from '@/components/ui/Swiper'
import classNames from 'classnames'
import { motion } from 'framer-motion'
import React from 'react'
import { SwiperProps, SwiperSlide } from 'swiper/react'

interface SmallItemSwiperProps extends SwiperProps {
  data: any[]
}

const SmallItemSwiper: React.FC<SmallItemSwiperProps> = ({
  data,
  ...props
}) => {
  return (
    <Swiper
      centeredSlides
      // centerInsufficientSlides
      loop
      hidePagination
      slidesPerView={5}
      loopedSlides={6}
      slidesPerGroup={1}
      spaceBetween={74}
      hideNavigation
      slideToClickedSlide
      isOverflowHidden={false}
      className="!py-6"
      // watchSlidesProgress={true}

      breakpoints={{
        1024: {
          slidesPerView: 5,
          spaceBetween: 74,
        },
      }}
      {...props}
    >
      {data &&
        data.map((item) => (
          <SwiperSlide key={item.name}>
            {({ isActive }) => {
              return (
                <motion.div
                  className={classNames(
                    isActive && 'shadow-[0px_2px_8px_rgba(33,148,255,0.6)]',
                    'flex h-[195px] w-[195px] items-center justify-center border-[0.5px] border-primary bg-gradient-to-b from-[rgba(111,194,255,0.6)] to-[rgba(0,35,68,0)] p-[30px]'
                  )}
                  variants={{
                    initial: {
                      scale: 1,
                    },
                    active: {
                      scale: 1.15,
                      speed: 300,
                    },
                  }}
                  animate={isActive ? 'active' : 'initial'}
                >
                  <Image
                    src={item.image}
                    alt={item.name}
                    width="100%"
                    height="100%"
                  />
                </motion.div>
              )
            }}
          </SwiperSlide>
        ))}
    </Swiper>
  )
}

export default SmallItemSwiper
