import { useWindowDimensions } from '@/hooks'
import { MapItem as MapItemType } from '@/interfaces'
import React from 'react'
import { If, Then } from 'react-if'
import MapItem from './MapItem'
import Image from '@/components/ui/Image'

interface MapListProps {
  id: string
  img: string
  Title: string
  Card: String
  ImageAvata: string
  nameAvata: string
  money: string
  money1: string
}
interface Chip {
  id:string
  name:string
  valuve:string
}

const MapList: React.FC<MapListProps> = ({
  id,
  img,
  Title,
  Card,
  ImageAvata,
  nameAvata,
  money,
  money1,
}) => {
  return (
   
    <a
      href="#"
      className="hover:bg-gray-100 dark:border-gray-700 dark:bg-gray-800 dark:hover:bg-gray-700 flex flex-col items-center rounded-lg border bg-white shadow-md md:max-w-xl md:flex-row mb-8"
    >
      <img
        className="h-96 w-full rounded-t-lg object-cover md:h-auto md:w-48 md:rounded-none md:rounded-l-lg"
        src={img}
      />
      <div className="flex flex-col justify-between p-4 leading-normal">
        <div className="mb-2 text-xl font-black text-black">{Title}</div>
        <div className="bg-white p-5">
        <div className="flex w-80 flex-wrap justify-center">
          <div className="chip chip-blue mr-2 w-20 rounded-xl bg-red-500 text-center">
            Webinar
          </div>
          <div className="chip chip-green mr-2 w-20 rounded-xl bg-sky-900 text-center">
            Certified
          </div>
          <div className="chip chip-red mr-2 w-20 rounded-xl bg-amber-600 text-center">
            1200 Point
          </div>
        </div>
        {/* <a href="#">
          <div className="mb-2 text-xl font-black text-black">{Title}</div>
        </a> */}
       
      </div>
        <div className="flex">
          <img className="" src={ImageAvata} />
          <div className="mb-2 text-xl font-black text-black">{nameAvata}</div>
        </div>
        <div className="flex justify-between">
          <div className="mb-2 text-xl font-black text-black">{money}</div>
          <div className="mb-2 text-xl font-black text-black line-through">{money1}</div>
        </div>
      </div>
    </a>
  )
}

export default React.memo(MapList)
