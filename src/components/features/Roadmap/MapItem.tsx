import { useWindowDimensions } from '@/hooks'
import { MapItem as MapItemType } from '@/interfaces'
import classNames from 'classnames'
import React from 'react'
import { Else, If, Then } from 'react-if'
import styles from './MapItem.module.css'

interface MapItemProps {
  item: MapItemType
}

const MapItem: React.FC<MapItemProps> = ({ item }) => {
  const { width } = useWindowDimensions()
  return (
    <If condition={width >= 1024}>
      <Then>
        <div className="group">
          <div className="relative grid grid-cols-2 items-center gap-x-[136px]">
            <div className="group-odd:order-2 group-odd:text-left group-even:order-1 group-even:text-right">
              <span className="text-gradient bg-clip-text pr-3 font-airStrike text-transparent lg:text-[32px] lg:leading-[54px] 2xl:text-[40px] 2xl:leading-[54px]">
                {item.title}
              </span>
            </div>
            <ul
              className={classNames(
                styles.list,
                'relative list-inside list-disc space-y-2 bg-black/20 py-4 pr-5 font-normal italic group-odd:order-1 group-even:order-2 lg:min-w-[298px] lg:pl-4 lg:text-xs lg:leading-5 2xl:min-w-[386px] 2xl:pl-6 2xl:text-base'
              )}
            >
              {item.desc.length &&
                item.desc.map((value, idx) => (
                  <li className="" key={idx}>
                    {value}
                  </li>
                ))}
            </ul>
            <div className="absolute left-1/2 z-[1] -translate-x-1/2 rounded-full border border-primary bg-red-300 bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] lg:h-[13px] lg:w-[13px] 2xl:h-4 2xl:w-4">
              <div className="absolute flex items-center -translate-x-full -translate-y-1/2 top-1/2 -left-1">
                <div className="arrow-left-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
                <div className="arrow-left-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
              </div>
              <div className="absolute flex items-center translate-x-full -translate-y-1/2 top-1/2 -right-1">
                <div className="arrow-right-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
                <div className="arrow-right-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
              </div>
            </div>
          </div>
        </div>
      </Then>
      <Else>
        <div className="relative z-10 ml-[5px] flex basis-full items-center justify-between">
          <div className="flex items-center">
            <div className="relative rounded-full border border-primary bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] xs:h-2 xs:w-2 lg:h-[13px] lg:w-[13px] 2xl:h-4 2xl:w-4"></div>
            <div className="flex items-center ml-1">
              <div className="arrow-right-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] xs:h-[6px] xs:w-[6px] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
              <div className="arrow-right-shape bg-gradient-to-br from-[#00EBB0_-3.99%] to-[#0481f2_111.72%] xs:h-[6px] xs:w-[6px] lg:h-2 lg:w-2 2xl:h-3 2xl:w-3"></div>
            </div>
          </div>

          <span className="text-gradient bg-clip-text pr-2 text-left font-airStrike text-transparent lg:text-[32px] lg:leading-[54px] 2xl:text-[40px] 2xl:leading-[54px]">
            {item.title}
          </span>

          <ul
            className={classNames(
              styles.list,
              'relative min-w-[260px] list-inside list-disc space-y-2 bg-black/20 font-normal italic xs:py-2 xs:pl-2 xs:text-[12px] xs:leading-5'
            )}
          >
            {item.desc.length &&
              item.desc.map((value, idx) => (
                <li className="" key={idx}>
                  {value}
                </li>
              ))}
          </ul>
        </div>
      </Else>
    </If>
  )
}

export default React.memo(MapItem)
