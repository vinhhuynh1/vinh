import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

import MapList from './MapList'
interface HeroBannerProps {
  id: string
  img: string
  Title: string
  Card: string
  ImageAvata: string
  nameAvata: string
  money: string
  money1: string
}
const data = [
  {
    id: 1,
    img: '/litepaper/icons/anh1.svg',
    Title:
      'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps and Chat Apps Building E-Commerce and Chat',
    Card: '',
    ImageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Su Rahman',
    money: 'Rp 120.000',
    money1: 'Rp 120.000',
  },
  {
    id: 2,
    img: '/litepaper/icons/anh1.svg',
    Title:
      'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps and Chat Apps Building E-Commerce and Chat',
    Card: '',
    ImageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Su Rahman',
    money: 'Rp 120.000',
    money1: 'Rp 120.000',
  },
  {
    id: 3,
    img: '/litepaper/icons/anh1.svg',
    Title:
      'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps and Chat Apps Building E-Commerce and Chat',
    Card: '',
    ImageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Su Rahman',
    money: 'Rp 120.000',
    money1: 'Rp 120.000',
  },
  {
    id: 4,
    img: '/litepaper/icons/anh1.svg',
    Title:
      'Full-Stack Laravel Flutter 2021: Building E-Commerce and Chat Apps and Chat Apps Building E-Commerce and Chat',
    Card: '',
    ImageAvata: '/litepaper/icons/Avatar.svg',
    nameAvata: 'Su Rahman',
    money: 'Rp 120.000',
    money1: 'Rp 120.000',
  },
]

const Roadmap: React.FC<HeroBannerProps> = () => {
  return (
    <Section>
      <div className="flex flex-wrap justify-around ">
        {data.map((mode) => {
          return (
            <MapList
              key={mode.Title}
              Title={mode.Title}
              img={mode.img}
              ImageAvata={mode.ImageAvata}
              nameAvata={mode.nameAvata}
              money={mode.money}
              money1={mode.money1}
            />
          )
        })}
      </div>
    </Section>
  )
}

export default Roadmap
