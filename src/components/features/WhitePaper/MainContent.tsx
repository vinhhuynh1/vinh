import React from 'react'

interface MainContentProps {}

const MainContent: React.FC<MainContentProps> = () => {
  return (
    <div className="flex-1 border-l border-white pl-[72px]">
      <h1 className="text-header-4">Earning Cap/Mechanics</h1>
      <div className="">
        <h2 className="mb-3 italic font-bold mt-7 font-TTNormsPro text-body-3">
          Daily Energy Cap
        </h2>
        <p className="text-body-5">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci amet
          volutpat lectus mi. Nec tempus quis sit enim viverra lectus sit.
          Tristique nunc porttitor aenean turpis vestibulum. Feugiat tempus
          consectetur mauris, tempus id. Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Orci amet volutpat lectus mi. Nec tempus quis sit
          enim viverra lectus sit. Tristique nunc porttitor aenean turpis
          vestibulum. Feugiat tempus consectetur mauris, tempus id.
        </p>
        <h2 className="mb-3 italic font-bold mt-7 font-TTNormsPro text-body-3">
          Earning Mechanics
        </h2>
        <p className="text-body-5">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Orci amet
          volutpat lectus mi. Nec tempus quis sit enim viverra lectus sit.
          Tristique nunc porttitor aenean turpis vestibulum. Feugiat tempus
          consectetur mauris, tempus id. Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Orci amet volutpat lectus mi. Nec tempus quis sit
          enim viverra lectus sit. Tristique nunc porttitor aenean turpis
          vestibulum. Feugiat tempus consectetur mauris, tempus id. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Orci amet volutpat lectus
          mi. Nec tempus quis sit enim viverra lectus sit. Tristique nunc
          porttitor aenean turpis vestibulum. Feugiat tempus consectetur mauris,
          tempus id. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Orci amet volutpat lectus mi. Nec tempus quis sit enim viverra lectus
          sit. Tristique nunc porttitor aenean turpis vestibulum. Feugiat tempus
          consectetur mauris, tempus id. Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Orci amet volutpat lectus mi. Nec tempus quis sit
          enim viverra lectus sit. Tristique nunc porttitor aenean turpis
          vestibulum. Feugiat tempus consectetur mauris, tempus id. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Orci amet volutpat lectus
          mi. Nec tempus quis sit enim viverra lectus sit. Tristique nunc
          porttitor aenean turpis vestibulum. Feugiat tempus consectetur mauris,
          tempus id. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Orci amet volutpat lectus mi. Nec tempus quis sit enim viverra lectus
          sit. Tristique nunc porttitor aenean turpis vestibulum. Feugiat tempus
          consectetur mauris, tempus id. Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Orci amet volutpat lectus mi. Nec tempus quis sit
          enim viverra lectus sit. Tristique nunc porttitor aenean turpis
          vestibulum. Feugiat tempus consectetur mauris, tempus id Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Orci amet volutpat lectus
          mi. Nec tempus quis sit enim viverra lectus sit. Tristique nunc
          porttitor aenean turpis vestibulum. Feugiat tempus consectetur mauris,
          tempus id. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          Orci amet volutpat lectus mi. Nec tempus quis sit enim viverra lectus
          sit. Tristique nunc porttitor aenean turpis vestibulum. Feugiat tempus
          consectetur mauris, tempus id. Lorem ipsum dolor sit amet, consectetur
          adipiscing elit. Orci amet volutpat lectus mi. Nec tempus quis sit
          enim viverra lectus sit. Tristique nunc porttitor aenean turpis
          vestibulum. Feugiat tempus consectetur mauris, tempus id. Lorem ipsum
          dolor sit amet, consectetur adipiscing elit. Orci amet volutpat lectus
          mi. Nec .
        </p>
      </div>
    </div>
  )
}

export default React.memo(MainContent)
