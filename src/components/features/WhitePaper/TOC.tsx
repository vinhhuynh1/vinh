import { ContentIcon } from '@/components/icons'
import React from 'react'

interface TOCProps {}

const TOC: React.FC<TOCProps> = () => {
  return (
    <div className="ml-16 w-[150px]">
      <h2 id="table-of-contents" className="flex items-center mb-3">
        <ContentIcon />
        <span className="ml-3 uppercase text-body-5">Contents</span>
      </h2>
      <ul className="space-y-3 font-normal text-body-5">
        <li className="text-[#bcbcbc]">Daily Energy Cap</li>
        <li className="text-white">Earning Mechanics</li>
        <li className="text-[#bcbcbc]">Lorem ipsum dolor </li>
        <li className="text-[#bcbcbc]">Anti-Cheating System</li>
        <li className="text-[#bcbcbc]">Tax and Fee System</li>
      </ul>
    </div>
  )
}

export default React.memo(TOC)
