import {
  DiscordRoundedIcon,
  TelegramRoundedIcon,
  TwitterRoundedIcon,
} from '@/components/icons'
import Image from '@/components/ui/Image'
import { Team } from '@/interfaces'
import Link from 'next/link'
import React from 'react'

interface TeamCardProps {
  data: Team
}

const TeamCard: React.FC<TeamCardProps> = ({ data }) => {
  return (
    <div className="team-card group bg-[#09072F] xs:skew-y-[3deg] xs:px-[6px] xs:pb-2 xs:pt-2 lg:min-w-[211px] lg:skew-y-[5deg] lg:px-2 lg:pb-4 2xl:px-3 2xl:pt-3 2xl:pb-[28px]">
      <div className="relative w-full shadow-[0px_1.5px_4.4px_rgba(28,92,152,0.25)] xs:h-[188px] lg:h-[248px] 2xl:h-[340px] ">
        <Image src={data.image} alt="team image" layout="fill" />
      </div>
      <div className="xs:skew-y-[-3deg] lg:mt-2 lg:skew-y-[-5deg] 2xl:mt-3">
        <div className="font-bold italic group-hover:text-[#001657] xs:text-[12px] xs:leading-5 lg:text-base lg:leading-7 2xl:text-body-2">
          {data.name}
        </div>
        <div className="italic group-hover:text-[#001657] xs:text-[8px] xs:leading-[14px] lg:text-[10px] lg:leading-4 2xl:text-sm">
          {data.position}
        </div>
      </div>
      <div className="flex-end mt-4 flex justify-end xs:skew-y-[-3deg] xs:space-x-[10px] lg:skew-y-[-5deg] lg:space-x-[14px] 2xl:space-x-6">
        <Link href="#!">
          <a className="text-[#bcbcbc] group-hover:text-[#001657] xs:h-3 xs:w-3 lg:h-4 lg:w-4 2xl:h-6 2xl:w-6">
            <TwitterRoundedIcon className="" />
          </a>
        </Link>
        <Link href="#!">
          <a className="text-[#bcbcbc] group-hover:text-[#001657] xs:h-3 xs:w-3 lg:h-4 lg:w-4 2xl:h-6 2xl:w-6">
            <DiscordRoundedIcon className="" />
          </a>
        </Link>
        <Link href="#!">
          <a className="text-[#bcbcbc] group-hover:text-[#001657] xs:h-3 xs:w-3 lg:h-4 lg:w-4 2xl:h-6 2xl:w-6">
            <TelegramRoundedIcon className="" />
          </a>
        </Link>
      </div>
    </div>
  )
}

export default TeamCard
