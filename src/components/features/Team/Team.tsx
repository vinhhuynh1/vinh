import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import React from 'react'
import TeamCard from './TeamCard'

interface TeamProps {}

const teamList = [
  {
    name: 'John Doe',
    position: 'CEO',
    image: '/team-1.png',
  },
  {
    name: 'Doe John',
    position: 'CTO',
    image: '/team-2.png',
  },
  {
    name: 'Foo bar',
    position: 'COO',
    image: '/team-3.png',
  },
  {
    name: 'Bar Foo',
    position: 'CFO',
    image: '/team-4.png',
  },
  {
    name: 'John Doe',
    position: 'CEO',
    image: '/team-1.png',
  },
  {
    name: 'Doe John',
    position: 'CTO',
    image: '/team-2.png',
  },
  {
    name: 'Foo bar',
    position: 'COO',
    image: '/team-3.png',
  },
  {
    name: 'Bar Foo',
    position: 'CFO',
    image: '/team-4.png',
  },
]

const Team: React.FC<TeamProps> = () => {
  return (
    <Section className="lg:mt-10 2xl:mt-[200px]">
      <SectionHeading title="meet the team" />
      <div className="xs:mt-4 lg:mt-9">
        <Swiper
          speed={1000}
          className="!overflow-y-visible xs:!pt-5 xs:!pb-14 lg:!pt-3 lg:!pb-20 2xl:!pb-[110px]"
          isOverflowHidden
          hideNavigation
          grabCursor
          breakpoints={{
            1600: {
              slidesPerView: 4,
              slidesPerGroup: 4,
              spaceBetween: 36,
            },
            1440: {
              slidesPerView: 4,
              slidesPerGroup: 4,
              spaceBetween: 36,
            },
            1366: {
              slidesPerView: 4,
              slidesPerGroup: 4,
              spaceBetween: 36,
            },
            1024: {
              slidesPerView: 4,
              slidesPerGroup: 4,
              spaceBetween: 12,
            },
            428: {
              slidesPerView: 2,
              slidesPerGroup: 1,
              spaceBetween: 10,
            },
          }}
        >
          {teamList.map((item, idx) => (
            <SwiperSlide key={idx}>
              <TeamCard data={item} />
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </Section>
  )
}

export default Team
