import Image from '@/components/ui/Image'
import classNames from 'classnames'
import React from 'react'

import styles from './RunningMode.module.css'

interface RunningModeItemProps {
  image: string
  title: string
  description: string
}

const RunningModeItem: React.FC<RunningModeItemProps> = ({
  image,
  title,
  description,
}) => {
  return (
    <div
      className={classNames(
        styles.item,
        'flex flex-col items-center lg:h-[307px] lg:w-[211px] lg:px-2 lg:pt-8 lg:pb-6 2xl:h-[465px] 2xl:w-[319px] 2xl:px-5 2xl:pt-14 2xl:pb-[38px]'
      )}
    >
      <div className="relative lg:h-[160px] lg:w-[160px] 2xl:h-[220px] 2xl:w-[220px]">
        <Image src={image} alt={title} layout="fill" />
      </div>
      <div className="font-airStrike text-primary lg:mt-[6px] lg:text-[20px] lg:leading-[34px] 2xl:mt-5 2xl:text-[32px] 2xl:leading-[52px]">
        {title}
      </div>
      <div className="mt-2 text-center italic lg:text-[10px] lg:leading-4 2xl:text-sm 2xl:leading-[22px]">
        {description}
      </div>
    </div>
  )
}

export default RunningModeItem
