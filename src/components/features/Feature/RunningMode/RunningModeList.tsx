import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'
import RunningModeItem from './RunningModeItem'

const runningModes = [
  {
    image: '/feature/running-mode/solo-mode-img.png',
    title: 'Solo mode',
    description:
      'Users are equipped with NFT Sneakers to earn tokens by moving. Energy is required to earn Papa Green Tokens (PGT).',
  },
  {
    image: '/feature/running-mode/clan-mode-img.png',
    title: 'clan mode',
    description:
      'A clan is created by 1 user and gathering 15 members. Top 3 clans which win the competition will be awarded.',
  },
  {
    image: '/feature/running-mode/marathon-mode-img.png',
    title: 'marathon mode',
    description:
      'Marathons are held daily with the number of 50 participants. Top 10 will receive token rewards and Participating NFT Badge.',
  },
]

const RunningModeList = () => {
  return (
    <Section className="mt-4">
      <SectionHeading title="Running mode" />

      <div className="mx-auto flex w-full justify-between lg:mt-9 lg:max-w-[732px] 2xl:mt-16 2xl:max-w-[1085px]">
        {runningModes.map((mode) => {
          return (
            <RunningModeItem
              key={mode.title}
              title={mode.title}
              image={mode.image}
              description={mode.description}
            />
          )
        })}
      </div>
    </Section>
  )
}

export default RunningModeList
