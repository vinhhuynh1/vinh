import Image from '@/components/ui/Image'
import classNames from 'classnames'
import React from 'react'

interface StrikingItemProps {
  data: { image: string; title: string; description: string }
  imageClassName?: string
}

const StrikingItem: React.FC<StrikingItemProps> = ({
  data,
  imageClassName,
}) => {
  return (
    <div className="flex items-center even:flex-row-reverse">
      <div className="flex items-center justify-center flex-1">
        <div className={classNames(imageClassName, 'relative')}>
          <Image src={data.image} alt={data.title} layout="fill" />
        </div>
      </div>
      <div className="flex-1">
        <div className="mx-auto w-[85%]">
          <div className="text-gradient bg-clip-text pr-2 font-airStrike text-[40px] leading-[54px] text-transparent">
            {data.title}
          </div>
          <div
            className="italic lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-4 2xl:text-base"
            dangerouslySetInnerHTML={{ __html: data.description }}
          ></div>
        </div>
      </div>
    </div>
  )
}

export default StrikingItem
