import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'
import StrikingItem from './StrikingItem'

const strikingFeatures = [
  {
    image: '/feature/striking-features/striking-marketplace.png',
    title: 'Marketplace',
    description:
      'On the in-app Marketplace, users can purchase, sell, or rent NFT Sneakers, Badges, and Gems. The Marketplace has a quick filtering and sorting feature for easier navigation.<br /> P-GO marketplace is an open area with no limitations on the web or Opensea, which reduces app loading times and increases the number of sales channels.<br /><br /> Buy to equip your exclusive Sneaker NFT and Gems from in-app marketplace, move outdoors and check your earning with PGO<br /> As you workout & burn calories, you keep earning!',
    imageClassName: 'lg:h-[389px] lg:w-[385px] 2xl:w-[496px] 2xl:h-[501px]',
  },
  {
    image: '/feature/striking-features/striking-wallet.png',
    title: 'wallet',
    description:
      'PGO has a built-in wallet, which has a built-in Swap function and backup function. The wallet offers multi-chain asset deposit & withdraw (SOL & BNB).<br /><br /> User can send NFT to another wallet address: you will transfer the NFT from the in-app wallet to the trading wallet so that the NFT can be sent to another wallet address. This form of transaction does not have to be bought and sold, please be sure to enter the correct wallet address to transfer to.<br /><br /> Also, PGO supports NFT PFP (Profile Photo) display for ETH and SOL.',
    imageClassName: 'lg:h-[430px] lg:w-[406px] 2xl:w-[524px] 2xl:h-[554px]',
  },
  {
    image: '/feature/striking-features/striking-inventory.png',
    title: 'inventory',
    description:
      'Gems, sneakers or shoe pieces, and recipe paper can all be stored in the Inventory. When you have more items, using inventory will help you manage your assets more effectively.<br /><br /> Please keep in mind that P-GO Inventory contains two distinct spaces:<br /> 1. NFT Sneakers share a space which allows users to have upto 256 NFT Sneakers<br /> 2. Gems, Mystery Box and Shoe Box share a space, which allows users to have upto 500 slots',
    imageClassName: 'lg:h-[394px] lg:w-[432px] 2xl:w-[557px] 2xl:h-[508px]',
  },
]

const StrikingList = () => {
  return (
    <Section className="mt-32">
      <SectionHeading title="STRIKING FEATURES" />

      <div className="mt-24 lg:space-y-20 2xl:space-y-24">
        {strikingFeatures.map((feature) => {
          return (
            <StrikingItem
              data={feature}
              key={feature.title}
              imageClassName={feature.imageClassName}
            />
          )
        })}
      </div>
    </Section>
  )
}

export default StrikingList
