import { AppleIcon, CHPlayIcon } from '@/components/icons'
import { Button } from '@/components/ui/Button'
import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

const SyncSmartWatch = () => {
  return (
    <Section className="lg:mt-[130px] 2xl:mt-[154px]">
      <SectionHeading title="Sync smart watch" />
      <div className="flex items-center mt-7">
        <div className="flex-1">
          <div className="mx-auto flex flex-col items-center justify-center lg:max-w-[415px] 2xl:max-w-[523px]">
            <div className="italic lg:text-xs lg:leading-5 2xl:text-base">
              Sync smart watch & fitness wearables with P-GO app and enjoy your
              running, walking, swimming, cycling, dancing, or working out
              activities.
            </div>
            <div className="mx-auto flex w-full justify-between lg:mt-3 lg:max-w-[356px] 2xl:mt-12 2xl:w-[436px]">
              <div className="flex flex-col items-center">
                <div className="relative mb-6 lg:h-[114px] lg:w-[114px] 2xl:h-[161px] 2xl:w-[161px]">
                  <Image
                    src="/feature/sync-smart-watch/apple-health.png"
                    alt="Apple Health"
                    layout="fill"
                  />
                </div>
                <Button
                  primary
                  LeftIcon={AppleIcon}
                  className="flex items-center text-sm font-medium"
                  iconClassName="mr-1 lg:h-4 lg:w-4 2xl:w-6 2xl:h-6"
                >
                  Apple store
                </Button>
              </div>
              <div className="flex flex-col items-center">
                <div className="relative mb-6 lg:h-[114px] lg:w-[114px] 2xl:h-[161px] 2xl:w-[161px]">
                  <Image
                    src="/feature/sync-smart-watch/google-fit.png"
                    alt="Apple Health"
                    layout="fill"
                  />
                </div>
                <Button
                  primary
                  LeftIcon={CHPlayIcon}
                  className="flex items-center text-sm font-medium"
                  iconClassName="mr-1 lg:h-4 lg:w-4 2xl:w-6 2xl:h-6"
                >
                  Google store
                </Button>
              </div>
            </div>
          </div>
        </div>
        <div className="flex-1 ">
          <div className="relative mx-auto lg:h-[367px] lg:w-[367px] 2xl:h-[496px] 2xl:w-[496px]">
            <Image
              src="/feature/sync-smart-watch/watches.png"
              alt=""
              layout="fill"
            />
          </div>
        </div>
      </div>
    </Section>
  )
}

export default React.memo(SyncSmartWatch)
