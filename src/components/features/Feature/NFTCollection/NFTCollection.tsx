import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'
import BoxCollection from './BoxCollection'
import GemCollection from './GemCollection'
import SneakerCollection from './SneakerCollection'

const NFTCollection = () => {
  return (
    <Section className="mt-[142px]">
      <SectionHeading
        title="NFT COLLECTION"
        desc="Explore P-GO NFT Collection and Updating striking features to unlock your full potiential"
      />
      <div className="mx-auto mt-14 lg:space-y-[100px] 2xl:space-y-[130px]">
        <SneakerCollection />
        <GemCollection />
        <BoxCollection />
      </div>
    </Section>
  )
}

export default NFTCollection
