import React from 'react'

interface CollectionProps {
  title: string
  desc: string
  itemData: {
    image: string
    title: string
  }
  image: string
}

const Collection: React.FC<CollectionProps> = ({
  title,
  desc,
  itemData,
  image,
}) => {
  return (
    <div className="flex items-center">
      <div className="flex-1"></div>
      <div className="flex-1"></div>
    </div>
  )
}

export default Collection
