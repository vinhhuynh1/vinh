import Image from '@/components/ui/Image'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import React from 'react'

const data = [
  {
    name: 'walker',
    image: '/feature/shoes/walker.png',
  },
  {
    name: 'jogger',
    image: '/feature/shoes/jogger.png',
  },
  {
    name: 'trainer',
    image: '/feature/shoes/trainer.png',
  },
  {
    name: 'runner',
    image: '/feature/shoes/runner.png',
  },
]

const SneakerCollection = () => {
  return (
    <div className="flex items-center">
      <div className="lg:w-1/2 2xl:w-[55%]">
        <div className="text-gradient bg-clip-text font-airStrike text-transparent lg:text-[40px] lg:leading-[54px]">
          sneaker
        </div>
        <div className="mb-3 italic lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-3 2xl:text-base">
          Purchase a NFT sneaker from in-app marketplace and then move outdoor.
          PGT/PMT will be paid per minute of movement, depending on 4 main
          factors: Sneaker Type, Sneaker&apos;s Efficiency Attribute,
          Sneaker&apos;s Comfort Attribute, Movement Speed.
        </div>
        <div>
          <Swiper
            hideNavigation
            hidePagination
            loop={false}
            slidesPerView={4}
            spaceBetween={30}
            className="!w-1/2 !flex-1"
            breakpoints={{
              1600: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 30,
              },
              1440: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 30,
              },
              1366: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 16,
              },
              1024: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 16,
              },
            }}
          >
            {data &&
              data.map((item) => (
                <SwiperSlide key={item.name}>
                  <div className="flex flex-col items-center justify-center border border-primary lg:h-[88px] lg:w-[88px] 2xl:h-[120px] 2xl:w-[120px]">
                    <div className="relative lg:h-[42px] lg:w-[71px] 2xl:h-[62px] 2xl:w-[104px]">
                      <Image src={item.image} alt={item.name} layout="fill" />
                    </div>
                    <div className="mt-[6px] font-airStrike text-primary lg:text-xs lg:leading-5 2xl:text-base 2xl:leading-[18px]">
                      {item.name}
                    </div>
                  </div>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      </div>
      <div className="ml-[50px] flex-1">
        <div className="relative lg:h-[279px] lg:w-[378px] 2xl:h-[363px] 2xl:w-[491px]">
          <Image src="/feature/shoes/big-shoe.png" alt="" layout="fill" />
          <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[120%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
        </div>
      </div>
    </div>
  )
}

export default SneakerCollection
