import Image from '@/components/ui/Image'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import React from 'react'

const data = [
  {
    name: 'luck',
    image: '/feature/gems/luck.png',
  },
  {
    name: 'resilience',
    image: '/feature/gems/resilience.png',
  },
  {
    name: 'comfort',
    image: '/feature/gems/comfort.png',
  },
  {
    name: 'efficiency',
    image: '/feature/gems/efficiency.png',
  },
]

const GemCollection = () => {
  return (
    <div className="flex items-center space-x-14">
      <div className="flex-1">
        <div className="relative lg:h-[244px] lg:w-[379px] 2xl:h-[292px] 2xl:w-[494px]">
          <Image src="/feature/gems/big-gem.png" alt="" layout="fill" />
          <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[120%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
        </div>
      </div>
      <div className="lg:w-1/2 2xl:w-[55%]">
        <div className="text-gradient bg-clip-text font-airStrike text-transparent lg:text-[40px] lg:leading-[54px]">
          gem
        </div>
        <div className="mb-3 italic lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-3 2xl:text-base">
          There are four types of Gems representing each Sneaker Attribute:
          Luck, Resilience, Comfort, Efficiency.
          <br />
          User can unlock Gem Socket to enhance their Sneaker Attribute by
          burning PMT and PGT.
        </div>
        <div>
          <Swiper
            hideNavigation
            hidePagination
            loop={false}
            slidesPerView={4}
            className="!flex-1"
            breakpoints={{
              1600: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 30,
              },
              1440: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 30,
              },
              1366: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 16,
              },
              1024: {
                slidesPerView: 4,
                slidesPerGroup: 4,
                spaceBetween: 16,
              },
            }}
          >
            {data &&
              data.map((item) => (
                <SwiperSlide key={item.name}>
                  <div className="flex flex-col items-center justify-center border border-primary lg:h-[88px] lg:w-[88px] 2xl:h-[120px] 2xl:w-[120px]">
                    <div className="relative lg:h-12 lg:w-12 2xl:h-[65px] 2xl:w-[65px]">
                      <Image src={item.image} alt={item.name} layout="fill" />
                    </div>
                    <div className="font-airStrike text-primary lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-[6px] 2xl:text-base 2xl:leading-[18px]">
                      {item.name}
                    </div>
                  </div>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      </div>
    </div>
  )
}

export default GemCollection
