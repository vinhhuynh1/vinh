import Image from '@/components/ui/Image'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import React from 'react'

const data = [
  {
    name: 'leather',
    image: '/feature/boxes/leather.png',
  },
  {
    name: 'plastic',
    image: '/feature/boxes/plastic.png',
  },
  {
    name: 'rubber',
    image: '/feature/boxes/rubber.png',
  },
  {
    name: 'coffee',
    image: '/feature/boxes/coffee.png',
  },
  {
    name: 'xenlulozo',
    image: '/feature/boxes/xenlulozo.png',
  },
]

const BoxCollection = () => {
  return (
    <div className="flex items-center">
      <div className="w-[55%]">
        <div className="text-gradient bg-clip-text font-airStrike text-transparent lg:text-[40px] lg:leading-[54px]">
          shoes box
        </div>
        <div className="mb-3 italic lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-3 2xl:text-base">
          The shoe piece has 5 levels of rarity following eco-friendly và
          high-quality sustainable sneakers: Leather, Plastic, Rubber, Coffee,
          Cellulose. Recycling shoebox requires token burning and a recipe paper
          for shoe molding.
        </div>
        <div>
          <Swiper
            hideNavigation
            hidePagination
            loop={false}
            slidesPerView={5}
            spaceBetween={24}
            className="!flex-1"
            breakpoints={{
              1600: {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 24,
              },
              1440: {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 24,
              },
              1366: {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 24,
              },
              1024: {
                slidesPerView: 5,
                slidesPerGroup: 5,
                spaceBetween: 12,
              },
            }}
          >
            {data &&
              data.map((item) => (
                <SwiperSlide key={item.name}>
                  <div className="flex flex-col items-center justify-center border border-primary lg:h-[76px] lg:w-[76px] 2xl:h-[100px] 2xl:w-[100px]">
                    <div className="relative lg:h-10 lg:w-[43px] 2xl:h-[52px] 2xl:w-[56px]">
                      <Image src={item.image} alt={item.name} layout="fill" />
                    </div>
                    <div className="font-airStrike text-primary lg:mt-1 lg:text-xs lg:leading-5 2xl:mt-[6px] 2xl:text-base 2xl:leading-[18px]">
                      {item.name}
                    </div>
                  </div>
                </SwiperSlide>
              ))}
          </Swiper>
        </div>
      </div>
      <div className="ml-[50px] flex-1">
        <div className="relative lg:h-[295px] lg:w-[322px] 2xl:h-[384px] 2xl:w-[419px]">
          <Image src="/feature/boxes/big-box.png" alt="" layout="fill" />
          <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[120%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
        </div>
      </div>
    </div>
  )
}

export default BoxCollection
