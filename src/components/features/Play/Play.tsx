import Dots from '@/components/ui/Dots'
import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import Swiper, { SwiperSlide } from '@/components/ui/Swiper'
import classNames from 'classnames'
import dynamic from 'next/dynamic'
import React from 'react'

import styles from './Play.module.css'

const ReactPlayer = dynamic(() => import('react-player'), { ssr: false })

const data = [
  {
    url: 'https://youtu.be/ApXoWvfEYVU',
    id: '1',
  },
  {
    url: 'https://youtu.be/pvuN_WvF1to',
    id: '2',
  },
]

const Play = () => {
  const [isPlaying, setIsPlaying] = React.useState(false)

  return (
    <Section hasPadding className="mt-20 lg:mt-[120px] lg:pb-0 2xl:pb-20">
      <SectionHeading
        title="P-GO PLAY"
        desc="PGO is a move-to-earn health and fitness app, using inbuilt Game-Fi and Social-Fi elements and rewards to motivate users to stay healthier."
      />
      <div className="relative mx-auto xs:mt-8 lg:mt-14">
        <Swiper
          className="!pb-16 lg:max-w-[765px] 2xl:max-w-[1084px]"
          centeredSlides
          // centerInsufficientSlides
          loop
          slidesPerView={1}
          loopedSlides={6}
          slidesPerGroup={1}
          spaceBetween={1}
          isOverflowHidden
          hideNavigation
        >
          {data &&
            data.map((item, idx) => (
              <SwiperSlide key={idx} className="relative">
                {({ isActive }) => {
                  return (
                    <div className="relative mx-auto xs:h-[222px] xs:w-[395px] lg:h-[340px] lg:w-[605px] 2xl:h-[489px] 2xl:w-[872px]">
                      <ReactPlayer
                        className="absolute top-0 left-0 react-player"
                        url={item.url}
                        width="100%"
                        height="100%"
                      />
                    </div>
                  )
                }}
              </SwiperSlide>
            ))}
        </Swiper>
      </div>
    </Section>
  )
}

export default Play
