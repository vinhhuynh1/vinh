import EChart from '@/components/ui/EChart'
import React from 'react'
import type { EChartsOption } from 'echarts'

interface DoughnutChartProps {}

const DoughnutChart: React.FC<DoughnutChartProps> = () => {
  const option: EChartsOption = {
    tooltip: {
      trigger: 'item',
      formatter: '{b} : {c} ({d}%)',
    },
    legend: {
      orient: 'vertical',
      right: '10',
      top: 'middle',
      textStyle: {
        color: '#fff',
        fontFamily: 'TT Norms Pro',
        fontStyle: 'italic',
        fontSize: '20',
        lineHeight: 34,
        align: 'center',
      },
      itemGap: 24,
      itemHeight: 32,
      itemWidth: 60,
      icon: 'react',
    },
    series: [
      {
        name: 'Access From',
        type: 'pie',
        radius: ['20%', '100%'],
        roseType: 'area',
        avoidLabelOverlap: false,
        labelLine: {
          show: false,
        },
        label: {
          show: false,
        },
        itemStyle: {
          // borderColor: 'rgb(21 9 86)',
          // borderWidth: 10,
        },
        center: ['30%', '50%'],
        data: [
          { value: 678, name: 'Search Engine' },
          { value: 735, name: 'Direct' },
          { value: 580, name: 'Email' },
          { value: 484, name: 'Union Ads' },
          { value: 245, name: 'Video Test' },
          { value: 421, name: 'Test 1' },
          { value: 523, name: 'Test 2' },
          { value: 126, name: 'Test 3' },
          { value: 289, name: 'Test 4' },
        ],
      },
    ],
    media: [
      {
        query: {
          maxWidth: 1024,
        },
        option: {
          legend: {
            right: '0',
            textStyle: {
              fontSize: '14',
              lineHeight: 24,
              align: 'center',
            },
            itemGap: 17,
            itemHeight: 22,
            itemWidth: 42,
            icon: 'react',
          },
        },
      },
    ],
  }

  return (
    <div className="mx-auto mt-[74px] lg:h-[483px] lg:w-[728px] 2xl:h-[670px] 2xl:w-[1027px]">
      <EChart option={option} />
    </div>
  )
}

export default DoughnutChart
