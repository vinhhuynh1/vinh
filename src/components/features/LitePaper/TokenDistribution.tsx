import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'
import DoughnutChart from './DoughnutChart'
import LineChart from './LineChart'

const TokenDistribution = () => {
  return (
    <Section className="mt-28 lg:mb-10 2xl:mb-[194px]">
      <SectionHeading title="Token Distribution" />
      <DoughnutChart />
      <LineChart />
    </Section>
  )
}

export default TokenDistribution
