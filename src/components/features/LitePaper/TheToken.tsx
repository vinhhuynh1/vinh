import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

interface TheTokenProps {}

const TheToken: React.FC<TheTokenProps> = () => {
  return (
    <Section className="mt-16">
      <SectionHeading
        title="The Token"
        desc="P-GO provides you PGT and PMT as a rewards for each calorie you burn."
      />
      <div>
        <div className="relative mx-auto mt-9 lg:h-[266px] lg:w-[700px] 2xl:h-[372px] 2xl:w-[980px]">
          <Image
            src="/litepaper/assets/the-token.png"
            alt="the token"
            layout="fill"
            unoptimized={true}
            containerClassName="pointer-events-none"
          />
          <div className="absolute z-10 -translate-x-1/2 text-center lg:top-[120px] 2xl:top-[167px]">
            <div className="font-airStrike text-primary lg:text-[32px] lg:leading-[54px] 2xl:text-[40px] 2xl:leading-[60px]">
              PGT
            </div>
            <div className="mt-2 font-TTNormsPro font-bold lg:text-xs lg:leading-5 2xl:text-[16px] 2xl:leading-[30px]">
              PAPA GREEN TOKEN
            </div>
          </div>
          <div className="absolute right-0 z-10 w-[239px] translate-x-1/2 text-center lg:top-[65px] 2xl:top-[76px]">
            <div className="font-airStrike text-primary lg:text-[32px] lg:leading-[54px] 2xl:text-[40px] 2xl:leading-[60px]">
              PGT
            </div>
            <div className="mt-2 font-TTNormsPro font-bold lg:text-xs lg:leading-5 2xl:text-[16px] 2xl:leading-[30px]">
              PAPA METAVER TOKEN
            </div>
          </div>
          <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[120%] w-[80%] -translate-x-1/2 -translate-y-1/2 rounded-full"></div>
        </div>
      </div>
    </Section>
  )
}

export default TheToken
