import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

interface TheTokenProps {}

const Tokenomic: React.FC<TheTokenProps> = () => {
  return (
    <Section className="mt-[72px]">
      <SectionHeading title="Tokenomic" />
      {/* <div className="relative mx-auto mt-20 max-w-[1296px]">
        <div className="flex justify-center">
          <div className="relative top-[155px] h-[241px] w-[175px]">
            <Image
              src="/litepaper/assets/line-1.png"
              alt="line"
              layout="fill"
            />
          </div>
          <div className="flex flex-col items-center border-t-[5px] border-primary bg-black/20 px-11 py-6">
            <div className="relative mx-auto h-[191px] w-[320px]">
              <Image
                src="/litepaper/assets/shoe.png"
                alt="shoe"
                layout="fill"
              />
            </div>
            <div className="mt-1 font-airStrike text-header-2">
              in-app marketplace
            </div>
          </div>
          <div className="relative top-[155px] h-[241px] w-[175px] -scale-x-[1]">
            <Image
              src="/litepaper/assets/line-1.png"
              alt="line"
              layout="fill"
            />
          </div>
        </div>
        <div className="mt-[97px] flex items-center justify-between">
          <div className="relative flex flex-col items-center border-t-[5px] border-primary bg-black/20 px-[77.5px] py-6 after:absolute after:bottom-0 after:left-1/2 after:h-[60px] after:w-[5px] after:-translate-x-1/2 after:translate-y-full after:bg-gradient-to-b after:from-[rgba(176,176,176,0)] after:to-[#2EFFB4]">
            <div className="font-airStrike text-header-2">Buy</div>
            <div className="relative mx-auto mt-4 h-[97px] w-[162px]">
              <Image
                src="/litepaper/assets/shoe.png"
                alt="shoe"
                layout="fill"
              />
            </div>
          </div>
          <div className="relative flex flex-col items-center border-t-[5px] border-primary bg-black/20 px-[70px] py-6 after:absolute after:bottom-0 after:left-1/2 after:h-[60px] after:w-[5px] after:-translate-x-1/2 after:translate-y-full after:bg-gradient-to-b after:from-[rgba(176,176,176,0)] after:to-[#2EFFB4]">
            <div className="font-airStrike text-header-2">Rent</div>
            <div className="mt-4 italic font-TTNormsPro text-body-1">
              30 % to renter <br /> 70% to owner
            </div>
          </div>
        </div>
        <div className="mt-[67px] flex items-center justify-between">
          <div className="flex flex-col items-center border-t-[5px] border-primary bg-black/20 py-6 px-[48px]">
            <div className="text-center font-airStrike text-header-2">
              move to <br /> earn
            </div>
            <div className="relative mx-auto mt-4 h-[90px] w-[138px]">
              <Image
                src="/litepaper/assets/coin-two.png"
                alt="shoe"
                layout="fill"
              />
            </div>
          </div>
          <div className="flex flex-col items-center border-t-[5px] border-primary bg-black/20 py-6 px-[48px]">
            <div className="text-center font-airStrike text-header-2">
              move to <br /> earn
            </div>
            <div className="relative mx-auto mt-4 h-[90px] w-[90px]">
              <Image
                src="/litepaper/assets/green-coin.png"
                alt="shoe"
                layout="fill"
              />
            </div>
          </div>
        </div>
        <div className="mt-[96px] flex justify-center">
          <div className="relative top-[-96px] h-[254px] w-[166px]">
            <Image
              src="/litepaper/assets/line-2.png"
              alt="line"
              layout="fill"
            />
          </div>
          <div className="flex flex-col items-center border-t-[5px] border-primary bg-black/20 px-[121px] py-6">
            <div className="relative mx-auto h-[118px] w-[381px]">
              <Image
                src="/litepaper/assets/coin-three.png"
                alt="shoe"
                layout="fill"
              />
            </div>
            <div className="mt-12 text-center font-airStrike text-header-2 leading-[50px]">
              in-app <br /> wallet/swap
            </div>
          </div>
          <div className="relative top-[-96px] ml-3 h-[254px] w-[166px] -scale-x-[1]">
            <Image
              src="/litepaper/assets/line-2.png"
              alt="line"
              layout="fill"
            />
          </div>
        </div>
      </div> */}
      <div className="relative mx-auto mt-14 lg:h-[1030px] lg:w-[880px] 2xl:h-[1445px] 2xl:w-[1231px]">
        <Image
          src="/litepaper/assets/tokenomic.png"
          alt=""
          layout="fill"
          unoptimized={true}
        />
        <div className="absolute top-1/2 left-1/2 h-[437px] w-[437px] -translate-x-1/2 -translate-y-1/2">
          <Image
            src="/litepaper/assets/tokenomic-finish.png"
            alt=""
            layout="fill"
            unoptimized={true}
          />
        </div>
      </div>
    </Section>
  )
}

export default Tokenomic
