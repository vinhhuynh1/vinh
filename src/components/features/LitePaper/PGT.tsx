import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

interface PGTProps {}

const PGT: React.FC<PGTProps> = () => {
  return (
    <Section className="lg:mt-[100px] 2xl:mt-[156px]">
      <SectionHeading
        title="PGT Tokenomic"
        desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porta in a diam justo lorem eget. Ornare mauris quis netus feugiat et praesent. Urna, tristique quisque cursus malesuada suspendisse consectetur. Hendrerit dictum id a amet dolor urna."
      />
      <div className="pointer-events-none relative mx-auto mt-16 lg:h-[445px] lg:w-[807px] 2xl:h-[661px] 2xl:w-[1214px]">
        <Image
          src="/litepaper/assets/pgt-tokenomic.png"
          alt="pgt tokenomic"
          layout="fill"
          unoptimized={true}
        />
        <div className="bg-gradient-purple absolute top-1/2 left-1/2 z-[-1] h-[110%] w-[80%] -translate-x-1/2 -translate-y-[40%] rounded-full"></div>
      </div>
    </Section>
  )
}

export default PGT
