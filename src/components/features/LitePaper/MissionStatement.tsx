import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import React from 'react'

import styles from '@/pages/litepaper/LitePaper.module.css'
import classNames from 'classnames'

interface MissionStatementProps {}

const MissionStatement: React.FC<MissionStatementProps> = () => {
  return (
    <Section className="mt-6">
      <SectionHeading
        title="Mission Statement"
        desc="We aim to build the new concept of the weathy healthy through the fitness metaverse, transform the definition of exercising, and reverse the negative perspective toward the NFTs and move-connect-earn community. "
      />
      <div className="mx-auto flex w-full items-center justify-between lg:mt-12 lg:max-w-[732px] 2xl:mt-16 2xl:max-w-[1046px]">
        <MissionItem
          imgPath="/litepaper/assets/tokenomic-finish.png"
          name="EASE OF USE"
          desc="Anyone can get started without understanding of NFTs or Crypto"
        />
        <DoubleArrowIcon />
        <MissionItem
          imgPath="/litepaper/assets/mission-coin.png"
          name="HABIT BUILDING"
          desc="PGO motivate users to get healthier by making physical activity more fun"
        />
        <DoubleArrowIcon />
        <MissionItem
          imgPath="/litepaper/assets/mission-income.png"
          name="INCREASE INCOME"
          desc="Using NFTs and crypto, PGO makes your workout financially rewarding"
        />
      </div>
    </Section>
  )
}

const MissionItem: React.FC<{
  imgPath: string
  name: string
  desc: string
}> = ({ imgPath, name, desc }) => {
  return (
    <div
      className={classNames(
        styles.missionItem,
        'relative flex flex-col items-center lg:h-[227px] lg:w-[211px] lg:px-2 lg:pb-3 lg:pt-6 2xl:h-[296px] 2xl:w-[282px] 2xl:px-[26px] 2xl:pt-[27px] 2xl:pb-6'
      )}
    >
      <div className="relative lg:h-[118px] lg:w-[118px] 2xl:h-40 2xl:w-40">
        <Image src={imgPath} alt={name} layout="fill" />
      </div>
      <div className="font-airStrike text-primary lg:text-base lg:leading-[34px] 2xl:text-2xl">
        {name}
      </div>
      <div className="mt-2 text-center italic lg:text-[10px] lg:leading-4 2xl:text-sm">
        {desc}
      </div>
    </div>
  )
}

const DoubleArrowIcon = () => {
  return (
    <div className="relative lg:h-[12px] lg:w-6 2xl:h-4 2xl:w-8">
      <Image
        src="/litepaper/icons/double-arrow.png"
        alt="double-arrow"
        layout="fill"
      />
    </div>
  )
}

export default MissionStatement
