import React from 'react'

import EChart from '@/components/ui/EChart'
import type { EChartsOption } from 'echarts'

interface LineChartProps {}

const LineChart: React.FC<LineChartProps> = () => {
  const option: EChartsOption = {
    tooltip: {
      trigger: 'axis',
      valueFormatter: (value) => value + '%',
    },
    legend: {
      right: '4%',
      textStyle: {
        color: '#fff',
        fontFamily: 'TT Norms Pro',
        fontStyle: 'italic',
        fontSize: '18',
        fontWeight: 'normal',
        lineHeight: 30,
        align: 'center',
      },
      itemGap: 82,
      icon: 'circle',
      data: ['Email', 'Union Ads', 'Video Ads'],
    },
    grid: {
      left: '3%',
      right: '4%',
      bottom: '3%',
      containLabel: true,
    },
    xAxis: {
      type: 'category',
      boundaryGap: ['10', '10'],
      axisLine: {
        lineStyle: {
          color: '#fff',
        },
      },
      axisLabel: {
        color: '#fff',
        fontFamily: 'TT Norms Pro',
        fontStyle: 'italic',
        fontSize: '18',
        fontWeight: 'normal',
        lineHeight: 30,
        margin: 10,
      },
      axisTick: {
        alignWithLabel: true,
        length: 9,
      },
      data: [
        '02/2022',
        '03/2022',
        '04/2022',
        '05/2022',
        '06/2022',
        '07/2022',
        '08/2022',
      ],
    },
    yAxis: {
      type: 'value',
      // max: 100,
      axisLabel: {
        color: '#fff',
        fontFamily: 'TT Norms Pro',
        fontStyle: 'italic',
        fontSize: '18',
        fontWeight: 'normal',
        lineHeight: 30,
        formatter: '{value}%',
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(0, 240, 255, 0.25)',
        },
      },
    },
    series: [
      {
        name: 'Email',
        type: 'line',
        data: [40, 65, 50, 80, 90, 72, 40],
      },
      {
        name: 'Union Ads',
        type: 'line',
        data: [20, 70, 40, 45, 75, 50, 15],
      },
      {
        name: 'Video Ads',
        type: 'line',
        data: [10, 30, 40, 50, 41, 82, 67],
      },
    ],
  }

  return (
    <div className="mx-auto lg:h-[546px] lg:w-[880px] 2xl:h-[732px] 2xl:w-[1140px]">
      <EChart option={option} />
    </div>
  )
}

export default LineChart
