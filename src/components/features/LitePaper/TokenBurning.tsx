import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import SectionHeading from '@/components/ui/SectionHeading'
import classNames from 'classnames'
import React from 'react'

const TokenBurning = () => {
  return (
    <Section className="mt-[120px]">
      <SectionHeading
        title="Token Burning"
        desc="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porta in a diam justo lorem eget. Ornare mauris quis netus feugiat et praesent. Urna, tristique quisque cursus malesuada suspendisse consectetur. Hendrerit dictum id a amet dolor urna."
      />
      <ul className="mx-auto lg:mt-10 lg:max-w-[723px] 2xl:mt-20 2xl:max-w-[930px]">
        <BurningItem
          title="SNEAKER REPAIR"
          containerClassName=""
          isHiddenIconPMT
        />
        <BurningItem
          title="SNEAKER SOCKETS UNLOCKING"
          containerClassName=""
          isHiddenIconPMT
        />
        <BurningItem
          title="LEVEL UP/MYSTERY BOX BOOSTING"
          containerClassName=""
          isHiddenIconPMT
        />
        <BurningItem
          title="CUSTOMIZATION FEE"
          containerClassName=""
          swap
          isHiddenIconPGT
        />
        <BurningItem title="LEVEL UP SNEAKERS" containerClassName="" />
        <BurningItem title="SHOE-MINTING" containerClassName="" />
        <BurningItem title="UPGRADE GEMS" containerClassName="" />
      </ul>
    </Section>
  )
}

const BurningItem: React.FC<{
  containerClassName?: string
  iconsContainerClassName?: string
  title: string
  swap?: boolean
  isHiddenIconPGT?: boolean
  isHiddenIconPMT?: boolean
}> = ({
  containerClassName,
  iconsContainerClassName,
  title,
  swap,
  isHiddenIconPGT,
  isHiddenIconPMT,
}) => (
  <li
    className={classNames(
      'mx-auto flex items-center border-b border-[rgba(0,240,255,0.25)] lg:py-3 2xl:py-4',
      containerClassName
    )}
  >
    <div className={classNames(iconsContainerClassName, 'flex space-x-2')}>
      {swap ? (
        <>
          <div
            className={classNames(
              'relative lg:h-[56px] lg:w-[56px] 2xl:h-[72px] 2xl:w-[72px]',
              {
                'invisible opacity-0': isHiddenIconPMT,
              }
            )}
          >
            <Image src="/litepaper/icons/pmt-coin.svg" alt="" layout="fill" />
          </div>
          <div
            className={classNames(
              'relative lg:h-[56px] lg:w-[56px] 2xl:h-[72px] 2xl:w-[72px]',
              {
                'invisible opacity-0': isHiddenIconPGT,
              }
            )}
          >
            <Image src="/litepaper/icons/pgt-coin.svg" alt="" layout="fill" />
          </div>
        </>
      ) : (
        <>
          <div
            className={classNames(
              'relative lg:h-[56px] lg:w-[56px] 2xl:h-[72px] 2xl:w-[72px]',
              {
                'invisible opacity-0': isHiddenIconPGT,
              }
            )}
          >
            <Image src="/litepaper/icons/pgt-coin.svg" alt="" layout="fill" />
          </div>
          <div
            className={classNames(
              'relative lg:h-[56px] lg:w-[56px] 2xl:h-[72px] 2xl:w-[72px]',
              {
                'invisible opacity-0': isHiddenIconPMT,
              }
            )}
          >
            <Image src="/litepaper/icons/pmt-coin.svg" alt="" layout="fill" />
          </div>
        </>
      )}
    </div>
    <span className="ml-[217px] font-bold italic lg:text-[20px] lg:leading-[34px] 2xl:text-2xl 2xl:leading-[47px]">
      {title}
    </span>
  </li>
)

export default TokenBurning
