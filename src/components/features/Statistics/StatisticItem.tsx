import { ArrowUpIcon } from '@/components/icons'
import { Statistic } from '@/interfaces'
import styles from './Statistic.module.css'
import React from 'react'
import classNames from 'classnames'

interface StatisticItemProps {
  item: Statistic
}

const StatisticItem: React.FC<StatisticItemProps> = ({ item, ...props }) => {
  return (
    <div
      className={classNames(
        styles.item,
        'lg:max-w-auto flex w-full flex-1 items-center border border-transparent xs:max-w-[396px] xs:py-12 xs:pl-[32px] lg:pb-5 lg:pt-[30px] lg:pl-[19px] 2xl:pb-7 2xl:pt-11 2xl:pl-[33px]'
      )}
      {...props}
    >
      <div className="xs:h-20 xs:w-20 lg:h-10 lg:w-10 2xl:h-[55px] 2xl:w-[55px]">
        {item.icon}
      </div>
      <div className="xs:ml-4 lg:ml-2">
        <div className="flex items-center">
          <div className="font-bold xs:mr-8 xs:text-[24px] xs:leading-[42px] lg:mr-1 lg:text-sm lg:leading-[22px] 2xl:mr-4 2xl:text-lg">
            {item.count}
          </div>

          <div className="flex items-center text-[#00F0FF]">
            <ArrowUpIcon />
            <span className="ml-1 xs:text-[20px] xs:leading-[34px] lg:text-xs lg:leading-[14px] 2xl:text-base">
              {item.growth}
            </span>
          </div>
        </div>
        <div className="leading-6 text-[#bcbcbc] xs:text-[16px] xs:leading-[34px] lg:text-[10px] lg:leading-4 2xl:text-sm">
          {item.unit}
        </div>
      </div>
    </div>
  )
}

export default StatisticItem
