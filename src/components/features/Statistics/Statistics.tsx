import {
  CaloriesIcon,
  CarbonIcon,
  CommunityIcon,
  DiscordIcon,
  DistanceIcon,
} from '@/components/icons'
import Section from '@/components/ui/Section'
import React from 'react'
import StatisticItem from './StatisticItem'

interface StatisticsProps {}

const statisticList = [
  {
    title: 'Distance Run',
    count: '28,456,799',
    growth: '0.67%',
    unit: 'DISTANCE RUN (m)',
    icon: <DistanceIcon />,
  },
  {
    title: 'Carbon Offset',
    count: '28,456,799',
    growth: '0.67%',
    unit: 'CARBON OFFSET (kg)',
    icon: <CarbonIcon />,
  },
  {
    title: 'Carlories Burned',
    count: '28,456,799',
    growth: '0.67%',
    unit: 'CALORIES BURNED (kcal)',
    icon: <CaloriesIcon />,
  },
]

const Statistics: React.FC<StatisticsProps> = () => {
  return (
    <Section>
      <div className="mt-3">
        <div className="text-gradient mx-auto bg-clip-text pr-4 text-center font-airStrike text-transparent xs:text-[32px] xs:leading-[54px] lg:max-w-[495px] lg:text-[40px] lg:leading-[56px] 2xl:max-w-[585px] 2xl:text-[48px] 2xl:leading-[62px]">
          JOIN TODAY AND TRACK YOUR WORKOUT!
        </div>
        <div className="mx-auto flex items-center justify-center xs:mt-8 xs:flex-col xs:space-y-12 lg:mt-12 lg:max-w-[732px] lg:flex-row lg:space-y-0 lg:space-x-[50px] 2xl:mt-32 2xl:max-w-[1080px] 2xl:space-x-24">
          {statisticList.map((item) => (
            <StatisticItem key={item.title} item={item} />
          ))}
        </div>
      </div>
    </Section>
  )
}

export default Statistics
