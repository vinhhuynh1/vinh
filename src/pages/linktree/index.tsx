import Section from '@/components/ui/Section'
import { LinkTreeIcon } from '@/components/icons'
import SectionHeading from '@/components/ui/SectionHeading'
import TopBanner from '@/components/ui/TopBanner'
import classNames from 'classnames'
import { NextPage } from 'next'
import Link, { LinkProps } from 'next/link'
import React from 'react'
import styles from './linktree.module.css'

interface LinkTreeProps {}

interface LinkTreeButtonProps extends LinkProps {
  title: string
  href: string
  Icon?: React.ComponentType<{ className: string }>
  // iconClassName?: string
}

const LinkTree: NextPage<LinkTreeProps> = () => {
  return (
    <React.Fragment>
      <TopBanner
        title="more than"
        highlight="a game"
        description="PGO aims to be an exclusive concept of the wealthy through health, a reliable app for fitness enthusiasts looking to boost their income with NFT rewards.<br />
By using Game-Fi and Social-Fi elements to make physical activities more fun and financially rewarding, PGO motivates users to get healthier. PGO Sneakers with sustainable quality also contribute to raise awareness on using more environmentally friendly materials."
      />
      <div className="mx-auto max-w-[875px]">
        <div className="mt-8">
          <SectionHeading title="general" />
          <div className="mt-11 flex flex-col space-y-6">
            <LinkTreeButton
              href="#!"
              title="More than a game"
              Icon={LinkTreeIcon.PlayIcon}
            />
            <LinkTreeButton
              href="#!"
              title="BINANCE RESEARCH"
              Icon={LinkTreeIcon.BinanceIcon}
            />
            <LinkTreeButton
              href="#!"
              title="WEBSITE"
              Icon={LinkTreeIcon.WebsiteIcon}
            />
            <LinkTreeButton
              href="#!"
              title="FAQ"
              Icon={LinkTreeIcon.HelpIcon}
            />
          </div>
        </div>
        <div className="mt-32">
          <SectionHeading title="the app" />
          <div className="mt-11 flex flex-col space-y-6">
            <LinkTreeButton
              href="#!"
              title="ios download"
              Icon={LinkTreeIcon.AppleStoreIcon}
            />
            <LinkTreeButton
              href="#!"
              title="android download"
              Icon={LinkTreeIcon.GoogleStoreIcon}
            />
          </div>
        </div>
        <div className="mt-32">
          <SectionHeading title="social" />
          <div className="mt-11 flex flex-col space-y-6">
            <LinkTreeButton
              href="#!"
              title="FACEBOOK"
              Icon={LinkTreeIcon.FacebookIcon}
            />
            <LinkTreeButton
              href="#!"
              title="instagram"
              Icon={LinkTreeIcon.InstagramIcon}
            />
            <LinkTreeButton
              href="#!"
              title="twitter"
              Icon={LinkTreeIcon.TwitterIcon}
            />
            <LinkTreeButton
              href="#!"
              title="discord"
              Icon={LinkTreeIcon.DiscordIcon}
            />
          </div>
        </div>
        <div className="mt-32 mb-32">
          <SectionHeading title="other" />
          <div className="mt-11 flex flex-col space-y-6">
            <LinkTreeButton
              href="#!"
              title="CALENDAR"
              Icon={LinkTreeIcon.CalendarIcon}
            />
            <LinkTreeButton
              href="#!"
              title="partner with p-go"
              Icon={LinkTreeIcon.PartnerIcon}
            />
            <LinkTreeButton
              href="#!"
              title="Strava Ambassador Application"
              Icon={LinkTreeIcon.ApplicationIcon}
            />
            <LinkTreeButton
              href="#!"
              title="APPLY 100 ACTIVATION CODES"
              Icon={LinkTreeIcon.QRCodeIcon}
            />
          </div>
        </div>
      </div>
    </React.Fragment>
  )
}

export default LinkTree

const LinkTreeButton: React.FC<LinkTreeButtonProps> = ({
  href,
  title,
  Icon,
}) => {
  return (
    <Link href={href}>
      <a
        className={classNames(
          styles.button,
          'diamond-btn after:diamond-btn relative flex w-full items-center justify-center py-[13px] text-xl font-bold uppercase leading-[38px] after:absolute after:inset-0 after:-z-10'
        )}
      >
        {title}
        {Icon && (
          <Icon className="absolute top-1/2 left-[60px] h-8 w-8 -translate-y-1/2" />
        )}
      </a>
    </Link>
  )
}
