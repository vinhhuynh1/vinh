import type { AppProps } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import nextI18nextConfig from '../../next-i18next.config'

import '@/styles/index.css'
import BaseLayout from '@/components/layout/'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <BaseLayout>
      <Component {...pageProps} />
    </BaseLayout>
  )
}

export default appWithTranslation(MyApp, nextI18nextConfig)
