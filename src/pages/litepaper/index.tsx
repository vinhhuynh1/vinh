import {
  MissionStatement,
  PGT,
  TheToken,
  TokenBurning,
  Tokenomic,
} from '@/components/features/LitePaper'
import TokenDistribution from '@/components/features/LitePaper/TokenDistribution'
import TopBanner from '@/components/ui/TopBanner'
import { NextPage } from 'next'
import React from 'react'

interface LitePaperProps {}

const LitePaper: NextPage<LitePaperProps> = () => {
  return (
    <React.Fragment>
      <TopBanner
        title="what is"
        highlight="p-go?"
        description="Move-to-Earn Health and Fitness App. <br /> PGO is a Web3 lifestyle app with inbuilt Game-Fi and Social-Fi elements.<br />
PGO allows users to earn crypto rewards by performing physical activity.<br />
Users equipped with sneaker NFTs can move outdoors to earn tokens and NFT rewards. PGO has a built-in wallet, swap, marketplace, and rental system that allow non-crypto users to onboard PGO."
      />
      <MissionStatement />
      <TheToken />
      <Tokenomic />
      <PGT />
      <TokenBurning />
      <TokenDistribution />
    </React.Fragment>
  )
}

export default LitePaper
