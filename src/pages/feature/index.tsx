import {
  NFTCollection,
  RunningModeList,
  StrikingFeatures,
  SyncSmartWatch,
} from '@/components/features/Feature'
import Image from '@/components/ui/Image'
import Section from '@/components/ui/Section'
import TopBanner from '@/components/ui/TopBanner'
import classNames from 'classnames'
import { NextPage } from 'next'
import React, { ReactNode } from 'react'

import styles from './Feature.module.css'

interface HowToPlayProps {}

const HowToPlay: NextPage<HowToPlayProps> = () => {
  return (
    <React.Fragment>
      <TopBanner
        title="Experience"
        highlight="<br />the unique features"
        description="With inbuilt excellent Game-Fi and Social-Fi elements, PGO aim to provide you not only unique but also eco-friendly features, to make sure you can maximize your experience."
      />
      <RunningModeList />
      <NFTCollection />
      <StrikingFeatures />
      <SyncSmartWatch />
    </React.Fragment>
  )
}

export default HowToPlay
