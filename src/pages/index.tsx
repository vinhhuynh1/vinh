import { NFTItems } from '@/components/features/NFTItem'
import Play from '@/components/features/Play'
import { Roadmap } from '@/components/features/Roadmap'
import Statistics from '@/components/features/Statistics'
import Team from '@/components/features/Team/Team'
import Backers from '@/components/ui/Backers'
import Community from '@/components/ui/Community'
import HeroBanner from '@/components/ui/HeroBanner'
import React from 'react'

const Home = () => {
  return (
    <React.Fragment>
      <HeroBanner />
      <Statistics />
      {/* <Play /> */}
      <Roadmap />
      <NFTItems />
      <Team />
      <Backers />
      <Community />
    </React.Fragment>
  )
}
export default Home
