import { TOC } from '@/components/features/WhitePaper'
import MainContent from '@/components/features/WhitePaper/MainContent'
import SearchBar from '@/components/ui/SearchBar'
import Section from '@/components/ui/Section'
import { SidebarMenu } from '@/components/ui/SidebarMenu'
import { NextPage } from 'next'
import React from 'react'

interface WhitePaperProps {}

const WhitePaper: NextPage<WhitePaperProps> = () => {
  return (
    <Section className="mt-[68px] mb-[100px] flex">
      <div className="w-[261px] py-7 pr-12">
        <SearchBar />
        <SidebarMenu />
      </div>
      <MainContent />
      <TOC />
    </Section>
  )
}

export default WhitePaper
